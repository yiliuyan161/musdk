from .models import Query
from datetime import datetime
from .utils import store_config, load_config
import numpy as np
import datetime as dt
from .cache import cache

import pandas as pd
import datetime as dt

    
class MuClient(object):
    def __init__(self, token=None):
        self.config=load_config()
        if token is None or len(token) < 1:
            self.token = self.config.get("token")
            if self.token is None:
                print("本地没有找到token缓存,请输入token!")
        else:
            self.token = token
            self.config['token'] = token
            store_config(self.config)

    def getBaseInfo(self,type: int, code: str, fields:str='all')->Query:
        """
        A股数据/股票列表接口
        http://www.mushuju.com/b_stocklist.html
        <参数>
        type:int 资产类型，取值范围：1|沪深京A股；2|沪深京B股；3|港股；4|美股；5|黄金；6|汇率；7|Reits；10|沪深指数；11|香港指数；12|全球指数；13|债券指数；20|场内基金；30|沪深债券；40|行业板块；41|概念板块；42|地域板块 Yes
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockType(self,flags: str, fields:str='all')->Query:
        """
        通用接口/沪深股票分类
        http://www.mushuju.com/b_hsclassification.html
        <参数>
        flags:long 分类标记，取值范围：1|上证A股；2|深证A股；3|北证A股；4|沪深京B股；5|新股；6|创业板；7|科创板；8|沪股通(港>沪)；9|深股通(港>深)；10|st股票；11|港股通(沪>港)；12|港股通(深>港) Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockType",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getHangyeCfg(self,bkcode: str, fields:str='all')->Query:
        """
        通用接口/板块成分股
        http://www.mushuju.com/b_bkcomponentstock.html
        <参数>
        bkcode:String 板块代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getHangyeCfg",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getIndicatorMoney(self,type: int, code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        通用接口/资金流向
        http://www.mushuju.com/b_moneyflow.html
        <参数>
        type:int 资产类型，取值范围：1|沪深京A股；3|港股；4|美股；10|沪深指数；20|场内基金；40|行业板块；41|概念板块；42|地域板块 Yes
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        price:float 最新价（元） 
        zdfd:float 涨跌幅度（%） 
        zded:float 涨跌额度（元） 
        jys:int 证券交易所 
        name:String 股票名称 
        z118:float 今日主力净流入-净占比（%） 
        zljlr:float 今日主力净流入-净额（元） 
        cddlr:float 今日超大单流入（元） 
        cddlc:float 今日超大单流出（元） 
        cddjlr:float 今日超大单净流入-净额（元） 
        z30:float 今日超大单净流入-净占比（%） 
        ddlr:float 今日大单流入（元） 
        ddlc:float 今日大单流出（元） 
        ddjlr:float 今日大单净流入-净额（元） 
        z33:float 今日大单净流入-净占比（%） 
        zdlr:float 今日中单流入（元） 
        zdlc:float 今日中单流出（元） 
        zdjlr:float 今日中单净流入-净额（元） 
        z36:float 今日中单净流入-净占比（%） 
        xdlr:float 今日小单流入（元） 
        xdlc:float 今日小单流出（元） 
        xdjlr:float 今日小单净流入-净额（元） 
        z39:float 今日小单净流入-净占比（%） 
        z71:float 3日涨跌幅（%） 
        z199:float 3日主力净流入-净额（元） 
        z200:float 3日主力净流入-净占比（%） 
        z201:float 3日超大单净流入-净额（元） 
        z202:float 3日超大单净流入-净占比（%） 
        z203:float 3日大单净流入-净额（元） 
        z204:float 3日大单净流入-净占比（%） 
        z205:float 3日中单净流入-净额（元） 
        z206:float 3日中单净流入-净占比（%） 
        z207:float 3日小单净流入-净额（元） 
        z208:float 3日小单净流入-净占比（%） 
        zf05:float 5日涨跌幅（%） 
        z98:float 5日主力净流入-净额（元） 
        z99:float 5日主力净流入-净占比（%） 
        z100:float 5日超大单净流入-净额（元） 
        z101:float 5日超大单净流入-净占比（%） 
        z102:float 5日大单净流入-净额（元） 
        z103:float 5日大单净流入-净占比（%） 
        z104:float 5日中单净流入-净额（元） 
        z105:float 5日中单净流入-净占比（%） 
        z106:float 5日小单净流入-净额（元） 
        z107:float 5日小单净流入-净占比（%） 
        zf10:float 10日涨幅（%） 
        z108:float 10日主力净流入-净额（元） 
        z109:float 10日主力净流入-净占比（%） 
        z110:float 10日超大单净流入-净额（元） 
        z111:float 10日超大单净流入-净占比（%） 
        z112:float 10日大单净流入-净额（元） 
        z113:float 10日大单净流入-净占比（%） 
        z114:float 10日中单净流入-净额（元） 
        z115:float 10日中单净流入-净占比（%） 
        z116:float 10日小单净流入-净额（元） 
        z117:float 10日小单净流入-净占比（%） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'price' : 'float', 'zdfd' : 'float', 'zded' : 'float', 'jys' : 'int', 'name' : 'str', 'z118' : 'float', 'zljlr' : 'float', 'cddlr' : 'float', 'cddlc' : 'float', 'cddjlr' : 'float', 'z30' : 'float', 'ddlr' : 'float', 'ddlc' : 'float', 'ddjlr' : 'float', 'z33' : 'float', 'zdlr' : 'float', 'zdlc' : 'float', 'zdjlr' : 'float', 'z36' : 'float', 'xdlr' : 'float', 'xdlc' : 'float', 'xdjlr' : 'float', 'z39' : 'float', 'z71' : 'float', 'z199' : 'float', 'z200' : 'float', 'z201' : 'float', 'z202' : 'float', 'z203' : 'float', 'z204' : 'float', 'z205' : 'float', 'z206' : 'float', 'z207' : 'float', 'z208' : 'float', 'zf05' : 'float', 'z98' : 'float', 'z99' : 'float', 'z100' : 'float', 'z101' : 'float', 'z102' : 'float', 'z103' : 'float', 'z104' : 'float', 'z105' : 'float', 'z106' : 'float', 'z107' : 'float', 'zf10' : 'float', 'z108' : 'float', 'z109' : 'float', 'z110' : 'float', 'z111' : 'float', 'z112' : 'float', 'z113' : 'float', 'z114' : 'float', 'z115' : 'float', 'z116' : 'float', 'z117' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'price': '最新价（元）', 'zdfd': '涨跌幅度（%）', 'zded': '涨跌额度（元）', 'jys': '证券交易所', 'name': '股票名称', 'z118': '今日主力净流入-净占比（%）', 'zljlr': '今日主力净流入-净额（元）', 'cddlr': '今日超大单流入（元）', 'cddlc': '今日超大单流出（元）', 'cddjlr': '今日超大单净流入-净额（元）', 'z30': '今日超大单净流入-净占比（%）', 'ddlr': '今日大单流入（元）', 'ddlc': '今日大单流出（元）', 'ddjlr': '今日大单净流入-净额（元）', 'z33': '今日大单净流入-净占比（%）', 'zdlr': '今日中单流入（元）', 'zdlc': '今日中单流出（元）', 'zdjlr': '今日中单净流入-净额（元）', 'z36': '今日中单净流入-净占比（%）', 'xdlr': '今日小单流入（元）', 'xdlc': '今日小单流出（元）', 'xdjlr': '今日小单净流入-净额（元）', 'z39': '今日小单净流入-净占比（%）', 'z71': '3日涨跌幅（%）', 'z199': '3日主力净流入-净额（元）', 'z200': '3日主力净流入-净占比（%）', 'z201': '3日超大单净流入-净额（元）', 'z202': '3日超大单净流入-净占比（%）', 'z203': '3日大单净流入-净额（元）', 'z204': '3日大单净流入-净占比（%）', 'z205': '3日中单净流入-净额（元）', 'z206': '3日中单净流入-净占比（%）', 'z207': '3日小单净流入-净额（元）', 'z208': '3日小单净流入-净占比（%）', 'zf05': '5日涨跌幅（%）', 'z98': '5日主力净流入-净额（元）', 'z99': '5日主力净流入-净占比（%）', 'z100': '5日超大单净流入-净额（元）', 'z101': '5日超大单净流入-净占比（%）', 'z102': '5日大单净流入-净额（元）', 'z103': '5日大单净流入-净占比（%）', 'z104': '5日中单净流入-净额（元）', 'z105': '5日中单净流入-净占比（%）', 'z106': '5日小单净流入-净额（元）', 'z107': '5日小单净流入-净占比（%）', 'zf10': '10日涨幅（%）', 'z108': '10日主力净流入-净额（元）', 'z109': '10日主力净流入-净占比（%）', 'z110': '10日超大单净流入-净额（元）', 'z111': '10日超大单净流入-净占比（%）', 'z112': '10日大单净流入-净额（元）', 'z113': '10日大单净流入-净占比（%）', 'z114': '10日中单净流入-净额（元）', 'z115': '10日中单净流入-净占比（%）', 'z116': '10日小单净流入-净额（元）', 'z117': '10日小单净流入-净占比（%）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getIndicatorMoney",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getDailyMarket(self,type: int, code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        通用接口/每日行情
        http://www.mushuju.com/b_dailymarket.html
        <参数>
        type:int 资产类型，取值范围：1|沪深京A股；2|沪深京B股；3|港股；4|美股；5|黄金；6|汇率；7|Reits；10|沪深指数；11|香港指数；12|全球指数；13|债券指数；20|场内基金；30|沪深债券；40|行业板块；41|概念板块；42|地域板块 Yes
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 股票名称 
        price:float 最新价（元） 
        zdfd:float 涨跌幅度（%） 
        zded:float 涨跌额度（元） 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        zhfu:float 振幅（%） 
        hslv:float 换手率（%） 
        sjlv:float 市净率（%） 
        dsyl:float 市盈率（动态） 
        jsyl:float 市盈率(静) 
        ttmsyl:float 市盈率(TTM) 
        ztj:float 涨停价（元） 
        dtj:float 跌停价（元） 
        jjia:float 均价（元） 
        lbi:float 量比 
        zgj:float 最高价（元） 
        zdj:float 最低价（元） 
        jrkpj:float 今日开盘价（元） 
        zrspj:float 昨日收盘价（元） 
        ssdate:String 上市日期 
        weibi:float 委比（%） 
        wpan:float 外盘（手） 
        npan:float 内盘（手） 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        mgsy:float 每股收益（元） 
        zf05:float 5日涨幅（%） 
        zf10:float 10日涨幅（%） 
        zf20:float 20日涨幅（%） 
        zf60:float 60日涨幅（%） 
        zfy:float 今年以来涨幅（%） 
        zys:float 总营收（元） 
        zystb:float 总营收同比（%） 
        jzc:float 净资产 
        jlr:float 净利润 
        mlil:float 毛利率 
        jlil:float 净利率 
        fzl:float 负债率 
        mgwfplr:float 每股未分配利润 
        mgjzc:float 每股净资产 
        mggjj:float 每股公积金（元） 
        zljlr:float 今日主力净流入（元） 
        cddlr:float 今日超大单流入（元） 
        cddlc:float 今日超大单流出（元） 
        cddjlr:float 今日超大单净流入（元） 
        ddlr:float 今日大单流入（元） 
        ddlc:float 今日大单流出（元） 
        ddjlr:float 今日大单净流入（元） 
        zdlr:float 今日中单流入（元） 
        zdlc:float 今日中单流出（元） 
        zdjlr:float 今日中单净流入（元） 
        xdlr:float 今日小单流入（元） 
        xdlc:float 今日小单流出（元） 
        xdjlr:float 今日小单净流入（元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'price' : 'float', 'zdfd' : 'float', 'zded' : 'float', 'cjl' : 'float', 'cje' : 'float', 'zhfu' : 'float', 'hslv' : 'float', 'sjlv' : 'float', 'dsyl' : 'float', 'jsyl' : 'float', 'ttmsyl' : 'float', 'ztj' : 'float', 'dtj' : 'float', 'jjia' : 'float', 'lbi' : 'float', 'zgj' : 'float', 'zdj' : 'float', 'jrkpj' : 'float', 'zrspj' : 'float', 'ssdate' : 'str', 'weibi' : 'float', 'wpan' : 'float', 'npan' : 'float', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'mgsy' : 'float', 'zf05' : 'float', 'zf10' : 'float', 'zf20' : 'float', 'zf60' : 'float', 'zfy' : 'float', 'zys' : 'float', 'zystb' : 'float', 'jzc' : 'float', 'jlr' : 'float', 'mlil' : 'float', 'jlil' : 'float', 'fzl' : 'float', 'mgwfplr' : 'float', 'mgjzc' : 'float', 'mggjj' : 'float', 'zljlr' : 'float', 'cddlr' : 'float', 'cddlc' : 'float', 'cddjlr' : 'float', 'ddlr' : 'float', 'ddlc' : 'float', 'ddjlr' : 'float', 'zdlr' : 'float', 'zdlc' : 'float', 'zdjlr' : 'float', 'xdlr' : 'float', 'xdlc' : 'float', 'xdjlr' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '股票名称', 'price': '最新价（元）', 'zdfd': '涨跌幅度（%）', 'zded': '涨跌额度（元）', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'zhfu': '振幅（%）', 'hslv': '换手率（%）', 'sjlv': '市净率（%）', 'dsyl': '市盈率（动态）', 'jsyl': '市盈率(静)', 'ttmsyl': '市盈率(TTM)', 'ztj': '涨停价（元）', 'dtj': '跌停价（元）', 'jjia': '均价（元）', 'lbi': '量比', 'zgj': '最高价（元）', 'zdj': '最低价（元）', 'jrkpj': '今日开盘价（元）', 'zrspj': '昨日收盘价（元）', 'ssdate': '上市日期', 'weibi': '委比（%）', 'wpan': '外盘（手）', 'npan': '内盘（手）', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'mgsy': '每股收益（元）', 'zf05': '5日涨幅（%）', 'zf10': '10日涨幅（%）', 'zf20': '20日涨幅（%）', 'zf60': '60日涨幅（%）', 'zfy': '今年以来涨幅（%）', 'zys': '总营收（元）', 'zystb': '总营收同比（%）', 'jzc': '净资产', 'jlr': '净利润', 'mlil': '毛利率', 'jlil': '净利率', 'fzl': '负债率', 'mgwfplr': '每股未分配利润', 'mgjzc': '每股净资产', 'mggjj': '每股公积金（元）', 'zljlr': '今日主力净流入（元）', 'cddlr': '今日超大单流入（元）', 'cddlc': '今日超大单流出（元）', 'cddjlr': '今日超大单净流入（元）', 'ddlr': '今日大单流入（元）', 'ddlc': '今日大单流出（元）', 'ddjlr': '今日大单净流入（元）', 'zdlr': '今日中单流入（元）', 'zdlc': '今日中单流出（元）', 'zdjlr': '今日中单净流入（元）', 'xdlr': '今日小单流入（元）', 'xdlc': '今日小单流出（元）', 'xdjlr': '今日小单净流入（元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getDailyMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getMinuteKLine(self,type: int, code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        通用接口/分线数据
        http://www.mushuju.com/b_branchdata.html
        <参数>
        type:int 资产类型，取值范围：1|沪深京A股；2|沪深京B股；3|港股；4|美股；20|场内基金；30|沪深债券 Yes
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 分时时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        cjjj:float 成交均价 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'cjjj' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '分时时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'cjjj': '成交均价'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getMinuteKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getHourKLine(self,type: int, code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        通用接口/时线数据
        http://www.mushuju.com/b_timelinedata.html
        <参数>
        type:int 资产类型，取值范围：1|沪深京A股；2|沪深京B股；3|港股；4|美股；10|沪深指数；11|香港指数；12|全球指数；13|债券指数；20|场内基金；30|沪深债券 Yes
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        ktype:int K线类别，取值范围：5|5分钟；15|15分钟；30|30分钟；60|60分钟 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getHourKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getDayKLine(self,type: int, code: str, ktype: int, fq: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        通用接口/日线数据
        http://www.mushuju.com/b_dailydata.html
        <参数>
        type:int 资产类型，取值范围：1|沪深京A股；2|沪深京B股；3|港股；4|美股；5|黄金；6|汇率；7|Reits；10|沪深指数；11|香港指数；12|全球指数；13|债券指数；20|场内基金；30|沪深债券；40|行业板块；41|概念板块；42|地域板块 Yes
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        fq:int 复权信息，取值范围：0|不复权；1|前复权；2|后复权 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getDayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHSABaseInfo(self,code: str, fields:str='all')->Query:
        """
        A股数据/A股列表
        http://www.mushuju.com/b_asharelist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHSABaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHSADailyMarket(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        A股数据/每日行情
        http://www.mushuju.com/b_adailymarket.html
        <参数>
        code:String 股票代码，code参数可以从【沪深京股票->A股->A股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 股票名称 
        price:float 最新价（元） 
        zdfd:float 涨跌幅度（%） 
        zded:float 涨跌额度（元） 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        zhfu:float 振幅（%） 
        hslv:float 换手率（%） 
        sjlv:float 市净率（%） 
        dsyl:float 市盈率（动态） 
        jsyl:float 市盈率(静) 
        ttmsyl:float 市盈率(TTM) 
        ztj:float 涨停价（元） 
        dtj:float 跌停价（元） 
        jjia:float 均价（元） 
        lbi:float 量比 
        zgj:float 最高价（元） 
        zdj:float 最低价（元） 
        jrkpj:float 今日开盘价（元） 
        zrspj:float 昨日收盘价（元） 
        ssdate:String 上市日期 
        weibi:float 委比（%） 
        wpan:float 外盘（手） 
        npan:float 内盘（手） 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        mgsy:float 每股收益（元） 
        zf05:float 5日涨幅（%） 
        zf10:float 10日涨幅（%） 
        zf20:float 20日涨幅（%） 
        zf60:float 60日涨幅（%） 
        zfy:float 今年以来涨幅（%） 
        zys:float 总营收（元） 
        zystb:float 总营收同比（%） 
        jzc:float 净资产 
        jlr:float 净利润 
        mlil:float 毛利率 
        jlil:float 净利率 
        fzl:float 负债率 
        mgwfplr:float 每股未分配利润 
        mgjzc:float 每股净资产 
        mggjj:float 每股公积金（元） 
        zljlr:float 今日主力净流入（元） 
        cddlr:float 今日超大单流入（元） 
        cddlc:float 今日超大单流出（元） 
        cddjlr:float 今日超大单净流入（元） 
        ddlr:float 今日大单流入（元） 
        ddlc:float 今日大单流出（元） 
        ddjlr:float 今日大单净流入（元） 
        zdlr:float 今日中单流入（元） 
        zdlc:float 今日中单流出（元） 
        zdjlr:float 今日中单净流入（元） 
        xdlr:float 今日小单流入（元） 
        xdlc:float 今日小单流出（元） 
        xdjlr:float 今日小单净流入（元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'price' : 'float', 'zdfd' : 'float', 'zded' : 'float', 'cjl' : 'float', 'cje' : 'float', 'zhfu' : 'float', 'hslv' : 'float', 'sjlv' : 'float', 'dsyl' : 'float', 'jsyl' : 'float', 'ttmsyl' : 'float', 'ztj' : 'float', 'dtj' : 'float', 'jjia' : 'float', 'lbi' : 'float', 'zgj' : 'float', 'zdj' : 'float', 'jrkpj' : 'float', 'zrspj' : 'float', 'ssdate' : 'str', 'weibi' : 'float', 'wpan' : 'float', 'npan' : 'float', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'mgsy' : 'float', 'zf05' : 'float', 'zf10' : 'float', 'zf20' : 'float', 'zf60' : 'float', 'zfy' : 'float', 'zys' : 'float', 'zystb' : 'float', 'jzc' : 'float', 'jlr' : 'float', 'mlil' : 'float', 'jlil' : 'float', 'fzl' : 'float', 'mgwfplr' : 'float', 'mgjzc' : 'float', 'mggjj' : 'float', 'zljlr' : 'float', 'cddlr' : 'float', 'cddlc' : 'float', 'cddjlr' : 'float', 'ddlr' : 'float', 'ddlc' : 'float', 'ddjlr' : 'float', 'zdlr' : 'float', 'zdlc' : 'float', 'zdjlr' : 'float', 'xdlr' : 'float', 'xdlc' : 'float', 'xdjlr' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '股票名称', 'price': '最新价（元）', 'zdfd': '涨跌幅度（%）', 'zded': '涨跌额度（元）', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'zhfu': '振幅（%）', 'hslv': '换手率（%）', 'sjlv': '市净率（%）', 'dsyl': '市盈率（动态）', 'jsyl': '市盈率(静)', 'ttmsyl': '市盈率(TTM)', 'ztj': '涨停价（元）', 'dtj': '跌停价（元）', 'jjia': '均价（元）', 'lbi': '量比', 'zgj': '最高价（元）', 'zdj': '最低价（元）', 'jrkpj': '今日开盘价（元）', 'zrspj': '昨日收盘价（元）', 'ssdate': '上市日期', 'weibi': '委比（%）', 'wpan': '外盘（手）', 'npan': '内盘（手）', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'mgsy': '每股收益（元）', 'zf05': '5日涨幅（%）', 'zf10': '10日涨幅（%）', 'zf20': '20日涨幅（%）', 'zf60': '60日涨幅（%）', 'zfy': '今年以来涨幅（%）', 'zys': '总营收（元）', 'zystb': '总营收同比（%）', 'jzc': '净资产', 'jlr': '净利润', 'mlil': '毛利率', 'jlil': '净利率', 'fzl': '负债率', 'mgwfplr': '每股未分配利润', 'mgjzc': '每股净资产', 'mggjj': '每股公积金（元）', 'zljlr': '今日主力净流入（元）', 'cddlr': '今日超大单流入（元）', 'cddlc': '今日超大单流出（元）', 'cddjlr': '今日超大单净流入（元）', 'ddlr': '今日大单流入（元）', 'ddlc': '今日大单流出（元）', 'ddjlr': '今日大单净流入（元）', 'zdlr': '今日中单流入（元）', 'zdlc': '今日中单流出（元）', 'zdjlr': '今日中单净流入（元）', 'xdlr': '今日小单流入（元）', 'xdlc': '今日小单流出（元）', 'xdjlr': '今日小单净流入（元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHSADailyMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHSAMinuteKLine(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        A股数据/分线数据
        http://www.mushuju.com/b_abranchdata.html
        <参数>
        code:String 股票代码，code参数可以从【沪深京股票->A股->A股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 分时时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        cjjj:float 成交均价 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'cjjj' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '分时时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'cjjj': '成交均价'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHSAMinuteKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHSAHourKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        A股数据/时线数据
        http://www.mushuju.com/b_atimelinedata.html
        <参数>
        code:String 股票代码，code参数可以从【沪深京股票->A股->A股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        ktype:int K线类别，取值范围：5|5分钟；15|15分钟；30|30分钟；60|60分钟 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHSAHourKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHSADayKLine(self,code: str, ktype: int, fq: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        A股数据/日线数据
        http://www.mushuju.com/b_adailydata.html
        <参数>
        code:String 股票代码，code参数可以从【沪深京股票->A股->A股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        fq:int 复权信息，取值范围：0|不复权；1|前复权；2|后复权 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHSADayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHSBBaseInfo(self,code: str, fields:str='all')->Query:
        """
        B股数据/B股列表
        http://www.mushuju.com/b_bsharelist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHSBBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHSBDailyMarket(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        B股数据/每日行情
        http://www.mushuju.com/b_bdailymarket.html
        <参数>
        code:String 股票代码，code参数可以从【沪深京股票->B股->B股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 股票名称 
        price:float 最新价（元） 
        zdfd:float 涨跌幅度（%） 
        zded:float 涨跌额度（元） 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        zhfu:float 振幅（%） 
        hslv:float 换手率（%） 
        sjlv:float 市净率（%） 
        dsyl:float 市盈率（动态） 
        jsyl:float 市盈率(静) 
        ttmsyl:float 市盈率(TTM) 
        ztj:float 涨停价（元） 
        dtj:float 跌停价（元） 
        jjia:float 均价（元） 
        lbi:float 量比 
        zgj:float 最高价（元） 
        zdj:float 最低价（元） 
        jrkpj:float 今日开盘价（元） 
        zrspj:float 昨日收盘价（元） 
        ssdate:String 上市日期 
        weibi:float 委比（%） 
        wpan:float 外盘（手） 
        npan:float 内盘（手） 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        mgsy:float 每股收益（元） 
        zf05:float 5日涨幅（%） 
        zf10:float 10日涨幅（%） 
        zf20:float 20日涨幅（%） 
        zf60:float 60日涨幅（%） 
        zfy:float 今年以来涨幅（%） 
        zys:float 总营收（元） 
        zystb:float 总营收同比（%） 
        jzc:float 净资产 
        jlr:float 净利润 
        mlil:float 毛利率 
        jlil:float 净利率 
        fzl:float 负债率 
        mgwfplr:float 每股未分配利润 
        mgjzc:float 每股净资产 
        mggjj:float 每股公积金（元） 
        zljlr:float 今日主力净流入（元） 
        cddlr:float 今日超大单流入（元） 
        cddlc:float 今日超大单流出（元） 
        cddjlr:float 今日超大单净流入（元） 
        ddlr:float 今日大单流入（元） 
        ddlc:float 今日大单流出（元） 
        ddjlr:float 今日大单净流入（元） 
        zdlr:float 今日中单流入（元） 
        zdlc:float 今日中单流出（元） 
        zdjlr:float 今日中单净流入（元） 
        xdlr:float 今日小单流入（元） 
        xdlc:float 今日小单流出（元） 
        xdjlr:float 今日小单净流入（元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'price' : 'float', 'zdfd' : 'float', 'zded' : 'float', 'cjl' : 'float', 'cje' : 'float', 'zhfu' : 'float', 'hslv' : 'float', 'sjlv' : 'float', 'dsyl' : 'float', 'jsyl' : 'float', 'ttmsyl' : 'float', 'ztj' : 'float', 'dtj' : 'float', 'jjia' : 'float', 'lbi' : 'float', 'zgj' : 'float', 'zdj' : 'float', 'jrkpj' : 'float', 'zrspj' : 'float', 'ssdate' : 'str', 'weibi' : 'float', 'wpan' : 'float', 'npan' : 'float', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'mgsy' : 'float', 'zf05' : 'float', 'zf10' : 'float', 'zf20' : 'float', 'zf60' : 'float', 'zfy' : 'float', 'zys' : 'float', 'zystb' : 'float', 'jzc' : 'float', 'jlr' : 'float', 'mlil' : 'float', 'jlil' : 'float', 'fzl' : 'float', 'mgwfplr' : 'float', 'mgjzc' : 'float', 'mggjj' : 'float', 'zljlr' : 'float', 'cddlr' : 'float', 'cddlc' : 'float', 'cddjlr' : 'float', 'ddlr' : 'float', 'ddlc' : 'float', 'ddjlr' : 'float', 'zdlr' : 'float', 'zdlc' : 'float', 'zdjlr' : 'float', 'xdlr' : 'float', 'xdlc' : 'float', 'xdjlr' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '股票名称', 'price': '最新价（元）', 'zdfd': '涨跌幅度（%）', 'zded': '涨跌额度（元）', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'zhfu': '振幅（%）', 'hslv': '换手率（%）', 'sjlv': '市净率（%）', 'dsyl': '市盈率（动态）', 'jsyl': '市盈率(静)', 'ttmsyl': '市盈率(TTM)', 'ztj': '涨停价（元）', 'dtj': '跌停价（元）', 'jjia': '均价（元）', 'lbi': '量比', 'zgj': '最高价（元）', 'zdj': '最低价（元）', 'jrkpj': '今日开盘价（元）', 'zrspj': '昨日收盘价（元）', 'ssdate': '上市日期', 'weibi': '委比（%）', 'wpan': '外盘（手）', 'npan': '内盘（手）', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'mgsy': '每股收益（元）', 'zf05': '5日涨幅（%）', 'zf10': '10日涨幅（%）', 'zf20': '20日涨幅（%）', 'zf60': '60日涨幅（%）', 'zfy': '今年以来涨幅（%）', 'zys': '总营收（元）', 'zystb': '总营收同比（%）', 'jzc': '净资产', 'jlr': '净利润', 'mlil': '毛利率', 'jlil': '净利率', 'fzl': '负债率', 'mgwfplr': '每股未分配利润', 'mgjzc': '每股净资产', 'mggjj': '每股公积金（元）', 'zljlr': '今日主力净流入（元）', 'cddlr': '今日超大单流入（元）', 'cddlc': '今日超大单流出（元）', 'cddjlr': '今日超大单净流入（元）', 'ddlr': '今日大单流入（元）', 'ddlc': '今日大单流出（元）', 'ddjlr': '今日大单净流入（元）', 'zdlr': '今日中单流入（元）', 'zdlc': '今日中单流出（元）', 'zdjlr': '今日中单净流入（元）', 'xdlr': '今日小单流入（元）', 'xdlc': '今日小单流出（元）', 'xdjlr': '今日小单净流入（元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHSBDailyMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHSBMinuteKLine(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        B股数据/分线数据
        http://www.mushuju.com/b_bbranchdata.html
        <参数>
        code:String 股票代码，code参数可以从【沪深京股票->B股->B股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 分时时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        cjjj:float 成交均价 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'cjjj' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '分时时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'cjjj': '成交均价'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHSBMinuteKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHSBHourKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        B股数据/时线数据
        http://www.mushuju.com/b_btimelinedata.html
        <参数>
        code:String 股票代码，code参数可以从【沪深京股票->B股->B股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        ktype:int K线类别，取值范围：5|5分钟；15|15分钟；30|30分钟；60|60分钟 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHSBHourKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHSBDayKLine(self,code: str, ktype: int, fq: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        B股数据/日线数据
        http://www.mushuju.com/b_bdailydata.html
        <参数>
        code:String 股票代码，code参数可以从【沪深京股票->B股->B股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        fq:int 复权信息，取值范围：0|不复权；1|前复权；2|后复权 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHSBDayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockReName(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/股票曾用名
        http://www.mushuju.com/b_stockformername.html
        <参数>
        code:String 股票代码，code参数可以从【沪深京股票->A股->A股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 更名时间 
        changeafterfn:String 公司名称 
        secucode:String 股票安全代码 
        securitytype:String 类别 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'changeafterfn' : 'str', 'secucode' : 'str', 'securitytype' : 'str'}
        column_name_dict = {'code': '股票代码', 'tdate': '更名时间', 'changeafterfn': '公司名称', 'secucode': '股票安全代码', 'securitytype': '类别'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockReName",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getCompanyInfo(self,code: str, fields:str='all')->Query:
        """
        基础数据/公司信息
        http://www.mushuju.com/b_company.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        changepercent:float 涨跌幅 
        close:float 最新收盘 
        listingdate:String 上市时间 
        ltgb:float 流通股本 
        ltsz:float 流通市值 
        marketvalue:float 总市值 
        pb:float 市净率 
        peration:float 市盈率 
        zgb:float 总股本 
        bk:String 板块描述 
        zycp:String 主营产品收入 
        businscope:String 经营范围 
        compprofile:String 公司简介 
        hxtc:String 核心题材 
        mainbusin:String 主营业务 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'changepercent' : 'float', 'close' : 'float', 'listingdate' : 'str', 'ltgb' : 'float', 'ltsz' : 'float', 'marketvalue' : 'float', 'pb' : 'float', 'peration' : 'float', 'zgb' : 'float', 'bk' : 'str', 'zycp' : 'str', 'businscope' : 'str', 'compprofile' : 'str', 'hxtc' : 'str', 'mainbusin' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'changepercent': '涨跌幅', 'close': '最新收盘', 'listingdate': '上市时间', 'ltgb': '流通股本', 'ltsz': '流通市值', 'marketvalue': '总市值', 'pb': '市净率', 'peration': '市盈率', 'zgb': '总股本', 'bk': '板块描述', 'zycp': '主营产品收入', 'businscope': '经营范围', 'compprofile': '公司简介', 'hxtc': '核心题材', 'mainbusin': '主营业务'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getCompanyInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockAccount(self,fields:str='all')->Query:
        """
        基础数据/股票账户
        http://www.mushuju.com/b_stockaccount.html
        <参数>
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        sdate:String 统计日期 
        xzsl:float 新增投资者数量（万户） 
        xzhb:float 新增投资者环比增长 
        xztb:float 新增投资者同比增长 
        qmsl:float 期末投资者总量（万户） 
        qmsla:float 期末投资者A股账号（万户） 
        qmslb:float 期末投资者B股账号（万户） 
        hszsz:float 沪深总市值（亿元） 
        hjzsz:float 沪深户均市值（万元） 
        szzs:float 上证指数收盘 
        szzdf:float 上证指数涨跌幅 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'sdate' : 'str', 'xzsl' : 'float', 'xzhb' : 'float', 'xztb' : 'float', 'qmsl' : 'float', 'qmsla' : 'float', 'qmslb' : 'float', 'hszsz' : 'float', 'hjzsz' : 'float', 'szzs' : 'float', 'szzdf' : 'float'}
        column_name_dict = {'sdate': '统计日期', 'xzsl': '新增投资者数量（万户）', 'xzhb': '新增投资者环比增长', 'xztb': '新增投资者同比增长', 'qmsl': '期末投资者总量（万户）', 'qmsla': '期末投资者A股账号（万户）', 'qmslb': '期末投资者B股账号（万户）', 'hszsz': '沪深总市值（亿元）', 'hjzsz': '沪深户均市值（万元）', 'szzs': '上证指数收盘', 'szzdf': '上证指数涨跌幅'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockAccount",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getTradeDate(self,mtype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/交易日历
        http://www.mushuju.com/b_calendar.html
        <参数>
        mtype:int 市场类型，取值范围：1|沪深京A股；2|港股；3|沪深港通-北向；4|沪深港通-南向 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        mtype:int 市场类型 
        tdate:String 交易时间 
        isopen:int 是否休市，1：半天休市，2：全体休市，3：交易 
        mkt:String 市场名称 
        holiday:String 休市原因 
        xs:String 休市时段，1：上午休市，2：下午休市 
        lastdate:String 前一个交易日 
        nextdate:String 后一个交易日 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'mtype' : 'int', 'tdate' : 'str', 'isopen' : 'int', 'mkt' : 'str', 'holiday' : 'str', 'xs' : 'str', 'lastdate' : 'str', 'nextdate' : 'str'}
        column_name_dict = {'mtype': '市场类型', 'tdate': '交易时间', 'isopen': '是否休市，1：半天休市，2：全体休市，3：交易', 'mkt': '市场名称', 'holiday': '休市原因', 'xs': '休市时段，1：上午休市，2：下午休市', 'lastdate': '前一个交易日', 'nextdate': '后一个交易日'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getTradeDate",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getChuQuanChuXi(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/除权除息
        http://www.mushuju.com/b_exdividend.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        tdate:String 除权除息日期 
        pgbl:float 若pgbl>0，则配股：每10股配(10*pgbl)股,配股价格(pgjg)元 
        pgjg:float 配股价格 
        pxbl:float 若pxbl>0，则派息：每10股派(10*pxbl)元 
        sgbl:float 若sgbl>0，则送股：每10股送(10*sgbl)股 
        cqcxtype:float 除权出息类型，按位与计算。(1&cqcxtype)==1，派息;(2&cqcxtype)==1，送股;(8&cqcxtype)==1，配股;(16&cqcxtype)==1，增发; 
        zfbl:float 若zfbl>0，则增发：(zfgs)万股;增发价格(zfjg)元 
        zfgs:float 增发股数(万股) 
        zfjg:float 增发价格(元) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'tdate' : 'str', 'pgbl' : 'float', 'pgjg' : 'float', 'pxbl' : 'float', 'sgbl' : 'float', 'cqcxtype' : 'float', 'zfbl' : 'float', 'zfgs' : 'float', 'zfjg' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'tdate': '除权除息日期', 'pgbl': '若pgbl>0，则配股：每10股配(10*pgbl)股,配股价格(pgjg)元', 'pgjg': '配股价格', 'pxbl': '若pxbl>0，则派息：每10股派(10*pxbl)元', 'sgbl': '若sgbl>0，则送股：每10股送(10*sgbl)股', 'cqcxtype': '除权出息类型，按位与计算。(1&cqcxtype)==1，派息;(2&cqcxtype)==1，送股;(8&cqcxtype)==1，配股;(16&cqcxtype)==1，增发;', 'zfbl': '若zfbl>0，则增发：(zfgs)万股;增发价格(zfjg)元', 'zfgs': '增发股数(万股)', 'zfjg': '增发价格(元)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getChuQuanChuXi",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getFuQuanYinZi(self,code: str, fq: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/复权因子
        http://www.mushuju.com/b_weightfactor.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        fq:int 复权类别，取值范围：1|前复权；2|后复权 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        type:int 资产类别，1|沪深A股 
        fq:int 复权信息 
        tdate:String 除权出息日期 
        fqfactor:String nan 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'type' : 'int', 'fq' : 'int', 'tdate' : 'str', 'fqfactor' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'type': '资产类别，1|沪深A股', 'fq': '复权信息', 'tdate': '除权出息日期', 'fqfactor': 'nan'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getFuQuanYinZi",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getZhiShuChengFenGu(self,mtype: int, fields:str='all')->Query:
        """
        基础数据/指数成分股
        http://www.mushuju.com/b_indexcomponent.html
        <参数>
        mtype:int 指数类别，取值范围：1|沪深300；2|上证50；3|中证500；4|科创50 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        mtype:int 指数类别 
        code:String 股票代码 
        indexname:String 指数名称 
        name:String 股票名称 
        bps:float 每股净资产(元) 
        changerate:float 涨跌幅 
        closeprice:float 最新价(元) 
        eps:float 每股收益(元) 
        freecap:float 流通市值(亿元) 
        freeshares:float 流通股本(亿股) 
        industry:String 主营行业 
        region:String 地区 
        roe:float 净资产收益率(%) 
        secucode:String 股票安全代码 
        totalshares:float 总股本(亿股) 
        weight:float 成分股权重 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'mtype' : 'int', 'code' : 'str', 'indexname' : 'str', 'name' : 'str', 'bps' : 'float', 'changerate' : 'float', 'closeprice' : 'float', 'eps' : 'float', 'freecap' : 'float', 'freeshares' : 'float', 'industry' : 'str', 'region' : 'str', 'roe' : 'float', 'secucode' : 'str', 'totalshares' : 'float', 'weight' : 'float'}
        column_name_dict = {'mtype': '指数类别', 'code': '股票代码', 'indexname': '指数名称', 'name': '股票名称', 'bps': '每股净资产(元)', 'changerate': '涨跌幅', 'closeprice': '最新价(元)', 'eps': '每股收益(元)', 'freecap': '流通市值(亿元)', 'freeshares': '流通股本(亿股)', 'industry': '主营行业', 'region': '地区', 'roe': '净资产收益率(%)', 'secucode': '股票安全代码', 'totalshares': '总股本(亿股)', 'weight': '成分股权重'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getZhiShuChengFenGu",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getJiGouDiaoYanTongJi(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/机构调研统计
        http://www.mushuju.com/b_researchstatistics.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 接待日期 
        secucode:String 股票代码简称 
        name:String 股票名称 
        jgsum:int 接待机构数量 
        receivewayexplain:String 接待方式 
        receptionist:String 接待人员 
        receiveplace:String 接待地点 
        noticedate:String 公告日期 
        numbernew:int 序号 
        orgtype:String 机构类型 
        receivestartdate:String 接待开始日期 
        receiveenddate:String 接待结束日期 
        receivetimeexplain:String 调研时间范围 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'secucode' : 'str', 'name' : 'str', 'jgsum' : 'int', 'receivewayexplain' : 'str', 'receptionist' : 'str', 'receiveplace' : 'str', 'noticedate' : 'str', 'numbernew' : 'int', 'orgtype' : 'str', 'receivestartdate' : 'str', 'receiveenddate' : 'str', 'receivetimeexplain' : 'str'}
        column_name_dict = {'code': '股票代码', 'tdate': '接待日期', 'secucode': '股票代码简称', 'name': '股票名称', 'jgsum': '接待机构数量', 'receivewayexplain': '接待方式', 'receptionist': '接待人员', 'receiveplace': '接待地点', 'noticedate': '公告日期', 'numbernew': '序号', 'orgtype': '机构类型', 'receivestartdate': '接待开始日期', 'receiveenddate': '接待结束日期', 'receivetimeexplain': '调研时间范围'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getJiGouDiaoYanTongJi",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getJiGouDiaoYanXiangXi(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/机构调研详细
        http://www.mushuju.com/b_researchdetailed.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        survey:String 调研机构 
        tdate:String 调研日期 
        name:String 股票名称 
        orgtype:String 机构类型 
        investigators:String 调研人员 
        receptionist:String 接待人员 
        receiveplace:String 接待地点 
        noticedate:String 公告日期 
        secucode:String 代码简称 
        receivewayexplain:String 接待方式 
        receivestartdate:String 接待开始日期 
        receiveenddate:String 接待结束日期 
        receivetimeexplain:String 调研时间范围 
        numbernew:int 序号 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'survey' : 'str', 'tdate' : 'str', 'name' : 'str', 'orgtype' : 'str', 'investigators' : 'str', 'receptionist' : 'str', 'receiveplace' : 'str', 'noticedate' : 'str', 'secucode' : 'str', 'receivewayexplain' : 'str', 'receivestartdate' : 'str', 'receiveenddate' : 'str', 'receivetimeexplain' : 'str', 'numbernew' : 'int'}
        column_name_dict = {'code': '股票代码', 'survey': '调研机构', 'tdate': '调研日期', 'name': '股票名称', 'orgtype': '机构类型', 'investigators': '调研人员', 'receptionist': '接待人员', 'receiveplace': '接待地点', 'noticedate': '公告日期', 'secucode': '代码简称', 'receivewayexplain': '接待方式', 'receivestartdate': '接待开始日期', 'receiveenddate': '接待结束日期', 'receivetimeexplain': '调研时间范围', 'numbernew': '序号'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getJiGouDiaoYanXiangXi",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getJiGouDiaoYanJiLv(self,code: str, fields:str='all')->Query:
        """
        基础数据/机构调研记录
        http://www.mushuju.com/b_researchrecords.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        survey:String 调研机构 
        tdate:String 调研日期 
        name:String 股票名称 
        orgtype:String 机构类型 
        investigators:String 调研人员 
        noticedate:String 公告日期 
        secucode:String 代码简称 
        content:String 机构调查详细内容，评估公司经营情况 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'survey' : 'str', 'tdate' : 'str', 'name' : 'str', 'orgtype' : 'str', 'investigators' : 'str', 'noticedate' : 'str', 'secucode' : 'str', 'content' : 'str'}
        column_name_dict = {'code': '股票代码', 'survey': '调研机构', 'tdate': '调研日期', 'name': '股票名称', 'orgtype': '机构类型', 'investigators': '调研人员', 'noticedate': '公告日期', 'secucode': '代码简称', 'content': '机构调查详细内容，评估公司经营情况'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getJiGouDiaoYanJiLv",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getLonghbDetail(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/龙虎榜个股详情
        http://www.mushuju.com/b_lhbdetailsindividual.html
        <参数>
        code:String 股票代码，code参数可以从【沪深京股票->A股->A股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 名称 
        explainstock:String 解读 
        closeprice:float 收盘价 
        changerate:float 涨跌幅 
        billboardnetamt:float 龙虎榜净买额(元) 
        billboardbuyamt:float 龙虎榜买入额(元) 
        billboardsellamt:float 龙虎榜卖出额(元) 
        billboarddealamt:float 龙虎榜成交额(元) 
        accumamount:float 市场总成交额(元) 
        dealnetratio:float 净买额占总成交比 
        dealamountratio:float 成交额占总成交比 
        turnoverrate:float 换手率 
        freemarketcap:float 流通市值(元) 
        explanation:String 上榜原因 
        trademarket:String 市场类型 
        secucode:String 股票标记 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'explainstock' : 'str', 'closeprice' : 'float', 'changerate' : 'float', 'billboardnetamt' : 'float', 'billboardbuyamt' : 'float', 'billboardsellamt' : 'float', 'billboarddealamt' : 'float', 'accumamount' : 'float', 'dealnetratio' : 'float', 'dealamountratio' : 'float', 'turnoverrate' : 'float', 'freemarketcap' : 'float', 'explanation' : 'str', 'trademarket' : 'str', 'secucode' : 'str'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '名称', 'explainstock': '解读', 'closeprice': '收盘价', 'changerate': '涨跌幅', 'billboardnetamt': '龙虎榜净买额(元)', 'billboardbuyamt': '龙虎榜买入额(元)', 'billboardsellamt': '龙虎榜卖出额(元)', 'billboarddealamt': '龙虎榜成交额(元)', 'accumamount': '市场总成交额(元)', 'dealnetratio': '净买额占总成交比', 'dealamountratio': '成交额占总成交比', 'turnoverrate': '换手率', 'freemarketcap': '流通市值(元)', 'explanation': '上榜原因', 'trademarket': '市场类型', 'secucode': '股票标记'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getLonghbDetail",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getLonghbActive(self,startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/龙虎榜每日活跃营业部
        http://www.mushuju.com/b_lhbdailyactive.html
        <参数>
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        operatedeptcode:String 营业机构代码 
        tdate:String 上榜日期 
        operatedeptname:String 机构名称 
        buyerappearnum:float 买入个股数 
        buystock:String 机构买入个股代码 
        orgnameabbr:String 机构简称 
        securitynameabbr:String 机构买入个股名称 
        sellerappearnum:float 卖出个股数 
        totalbuyamt:float 买入总金额(万) 
        totalnetamt:float 总买卖净额(万) 
        totalsellamt:float 卖出总金额(万) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'operatedeptcode' : 'str', 'tdate' : 'str', 'operatedeptname' : 'str', 'buyerappearnum' : 'float', 'buystock' : 'str', 'orgnameabbr' : 'str', 'securitynameabbr' : 'str', 'sellerappearnum' : 'float', 'totalbuyamt' : 'float', 'totalnetamt' : 'float', 'totalsellamt' : 'float'}
        column_name_dict = {'operatedeptcode': '营业机构代码', 'tdate': '上榜日期', 'operatedeptname': '机构名称', 'buyerappearnum': '买入个股数', 'buystock': '机构买入个股代码', 'orgnameabbr': '机构简称', 'securitynameabbr': '机构买入个股名称', 'sellerappearnum': '卖出个股数', 'totalbuyamt': '买入总金额(万)', 'totalnetamt': '总买卖净额(万)', 'totalsellamt': '卖出总金额(万)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getLonghbActive",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getLonghbJigou(self,startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/龙虎榜机构每日买卖统计
        http://www.mushuju.com/b_lhbdailysalesinstitutions.html
        <参数>
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 股票名称 
        accumamount:float 市场总成交额(万) 
        buyamt:float 机构买入总额(万) 
        buytimes:float 买方机构数 
        changerate:float 涨跌幅 
        closeprice:float 收盘价 
        d10closeadjchrate:float 上榜后10日涨跌幅 
        d1closeadjchrate:float 上榜后1日涨跌幅 
        d2closeadjchrate:float 上榜后2日涨跌幅 
        d5closeadjchrate:float 上榜后5日涨跌幅 
        explanation:String 上榜原因 
        freecap:float 流通市值(亿) 
        market:String 市场 
        netbuyamt:float 机构买入净额(万) 
        ratio:float 机构净买额占总成交额比 
        secucode:String 代码简写 
        sellamt:float 机构卖出总额(万) 
        selltimes:float 卖方机构数 
        turnoverrate:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'accumamount' : 'float', 'buyamt' : 'float', 'buytimes' : 'float', 'changerate' : 'float', 'closeprice' : 'float', 'd10closeadjchrate' : 'float', 'd1closeadjchrate' : 'float', 'd2closeadjchrate' : 'float', 'd5closeadjchrate' : 'float', 'explanation' : 'str', 'freecap' : 'float', 'market' : 'str', 'netbuyamt' : 'float', 'ratio' : 'float', 'secucode' : 'str', 'sellamt' : 'float', 'selltimes' : 'float', 'turnoverrate' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '股票名称', 'accumamount': '市场总成交额(万)', 'buyamt': '机构买入总额(万)', 'buytimes': '买方机构数', 'changerate': '涨跌幅', 'closeprice': '收盘价', 'd10closeadjchrate': '上榜后10日涨跌幅', 'd1closeadjchrate': '上榜后1日涨跌幅', 'd2closeadjchrate': '上榜后2日涨跌幅', 'd5closeadjchrate': '上榜后5日涨跌幅', 'explanation': '上榜原因', 'freecap': '流通市值(亿)', 'market': '市场', 'netbuyamt': '机构买入净额(万)', 'ratio': '机构净买额占总成交额比', 'secucode': '代码简写', 'sellamt': '机构卖出总额(万)', 'selltimes': '卖方机构数', 'turnoverrate': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getLonghbJigou",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getRzRjMarket(self,mtype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/两市融资融券
        http://www.mushuju.com/b_twomargintrading.html
        <参数>
        mtype:int 市场类型，取值范围：1|沪深两市；2|沪市；3|深市 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        mtype:int 市场类型 
        tdate:String 交易时间 
        rqchl:float 融券偿还量(股) 
        rqjmg:float 融券净卖出(股) 
        rqmcl:float 融券卖出量(股) 
        rqye:float 融券当日余额(元) 
        rqyl:float 融券当日余量(股) 
        rzche:float 融资偿还额(元) 
        rzjme:float 融资净买入(元) 
        rzmre:float 融资买入额(元) 
        rzrqye:float 融资融券余额(元) 
        rzrqyecz:float 融资融券余额差值(元) 
        rzye:float 融资当日余额(元) 
        rzyezb:float 融资当日余额占流通市值比 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'mtype' : 'int', 'tdate' : 'str', 'rqchl' : 'float', 'rqjmg' : 'float', 'rqmcl' : 'float', 'rqye' : 'float', 'rqyl' : 'float', 'rzche' : 'float', 'rzjme' : 'float', 'rzmre' : 'float', 'rzrqye' : 'float', 'rzrqyecz' : 'float', 'rzye' : 'float', 'rzyezb' : 'float'}
        column_name_dict = {'mtype': '市场类型', 'tdate': '交易时间', 'rqchl': '融券偿还量(股)', 'rqjmg': '融券净卖出(股)', 'rqmcl': '融券卖出量(股)', 'rqye': '融券当日余额(元)', 'rqyl': '融券当日余量(股)', 'rzche': '融资偿还额(元)', 'rzjme': '融资净买入(元)', 'rzmre': '融资买入额(元)', 'rzrqye': '融资融券余额(元)', 'rzrqyecz': '融资融券余额差值(元)', 'rzye': '融资当日余额(元)', 'rzyezb': '融资当日余额占流通市值比'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getRzRjMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getRzRjHangye(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/板块融资融券
        http://www.mushuju.com/b_bkmargintrading.html
        <参数>
        code:String 板块代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 板块代码 
        tdate:String 交易日期 
        btcode:int 板块类型 
        boardtype:String 板块类型名称 
        finbalance:float 融资-余额(元) 
        finbalanceratio:float 融资-余额占流通市值比 
        finbuyamt:float 融资-买入额(元) 
        finnetbuyamt:float 融券-净买入(元) 
        finrepayamt:float 融资-偿还额(元) 
        finnetsellamt:float 融券-净卖出(股) 
        loanbalance:float 融券-余额(元) 
        loanbalancevol:float 融券-余量(股) 
        loannetsellamt:float 融券-净卖额(元) 
        loanrepayvol:float 融券-偿还量(股) 
        loansellamt:float 融券-卖出额 
        loansellvol:float 融券-卖出量(股) 
        marginbalance:float 融资融券余额(元) 
        finbalancediff:float 融资融券余额差值(元) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'btcode' : 'int', 'boardtype' : 'str', 'finbalance' : 'float', 'finbalanceratio' : 'float', 'finbuyamt' : 'float', 'finnetbuyamt' : 'float', 'finrepayamt' : 'float', 'finnetsellamt' : 'float', 'loanbalance' : 'float', 'loanbalancevol' : 'float', 'loannetsellamt' : 'float', 'loanrepayvol' : 'float', 'loansellamt' : 'float', 'loansellvol' : 'float', 'marginbalance' : 'float', 'finbalancediff' : 'float'}
        column_name_dict = {'code': '板块代码', 'tdate': '交易日期', 'btcode': '板块类型', 'boardtype': '板块类型名称', 'finbalance': '融资-余额(元)', 'finbalanceratio': '融资-余额占流通市值比', 'finbuyamt': '融资-买入额(元)', 'finnetbuyamt': '融券-净买入(元)', 'finrepayamt': '融资-偿还额(元)', 'finnetsellamt': '融券-净卖出(股)', 'loanbalance': '融券-余额(元)', 'loanbalancevol': '融券-余量(股)', 'loannetsellamt': '融券-净卖额(元)', 'loanrepayvol': '融券-偿还量(股)', 'loansellamt': '融券-卖出额', 'loansellvol': '融券-卖出量(股)', 'marginbalance': '融资融券余额(元)', 'finbalancediff': '融资融券余额差值(元)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getRzRjHangye",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockRzRj(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/个股融资融券
        http://www.mushuju.com/b_ggmargintrading.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        tdate:String 交易时间 
        rqchl:float 融券偿还量(股) 
        rqjmg:float 融券净卖出(股) 
        rqmcl:float 融券卖出量(股) 
        rqye:float 融券当日余额(元) 
        rqyl:float 融券当日余量(股) 
        rzche:float 融资偿还额(元) 
        rzjme:float 融资净买入(元) 
        rzmre:float 融资买入额(元) 
        rzrqye:float 融资融券余额(元) 
        rzrqyecz:float 融资融券余额差值(元) 
        rzye:float 融资当日余额(元) 
        trademarket:String 交易所 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'tdate' : 'str', 'rqchl' : 'float', 'rqjmg' : 'float', 'rqmcl' : 'float', 'rqye' : 'float', 'rqyl' : 'float', 'rzche' : 'float', 'rzjme' : 'float', 'rzmre' : 'float', 'rzrqye' : 'float', 'rzrqyecz' : 'float', 'rzye' : 'float', 'trademarket' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'tdate': '交易时间', 'rqchl': '融券偿还量(股)', 'rqjmg': '融券净卖出(股)', 'rqmcl': '融券卖出量(股)', 'rqye': '融券当日余额(元)', 'rqyl': '融券当日余量(股)', 'rzche': '融资偿还额(元)', 'rzjme': '融资净买入(元)', 'rzmre': '融资买入额(元)', 'rzrqye': '融资融券余额(元)', 'rzrqyecz': '融资融券余额差值(元)', 'rzye': '融资当日余额(元)', 'trademarket': '交易所'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockRzRj",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getRzRjAccount(self,startDate: str, endDate: str, fields:str='all')->Query:
        """
        基础数据/融资融券账户信息
        http://www.mushuju.com/b_margintradingaccount.html
        <参数>
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        tdate:String 交易时间 
        pjdbbl:float 平均维持担保比例 
        rzye:float 融资余额(亿) 
        rzmre:float 融资买入额(亿) 
        jytzz:float 参与交易的投资者数量(人) 
        rjye:float 融券余额(亿) 
        rjmce:float 融券卖出额(亿) 
        fztzz:float 有融资融券负债的投资者数量(万名) 
        yyb:float 营业部数量(家) 
        jgtzz:float 机构投资者数量(家) 
        grtzz:float 个人投资者数量(万名) 
        zjgs:float 证券公司数量(家) 
        dbw:float 担保物总价值(亿) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'tdate' : 'str', 'pjdbbl' : 'float', 'rzye' : 'float', 'rzmre' : 'float', 'jytzz' : 'float', 'rjye' : 'float', 'rjmce' : 'float', 'fztzz' : 'float', 'yyb' : 'float', 'jgtzz' : 'float', 'grtzz' : 'float', 'zjgs' : 'float', 'dbw' : 'float'}
        column_name_dict = {'tdate': '交易时间', 'pjdbbl': '平均维持担保比例', 'rzye': '融资余额(亿)', 'rzmre': '融资买入额(亿)', 'jytzz': '参与交易的投资者数量(人)', 'rjye': '融券余额(亿)', 'rjmce': '融券卖出额(亿)', 'fztzz': '有融资融券负债的投资者数量(万名)', 'yyb': '营业部数量(家)', 'jgtzz': '机构投资者数量(家)', 'grtzz': '个人投资者数量(万名)', 'zjgs': '证券公司数量(家)', 'dbw': '担保物总价值(亿)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getRzRjAccount",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getReportNianBao(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        年报季报/业绩报表
        http://www.mushuju.com/b_performancereport.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 报告日期 
        name:String 股票名称 
        assigndscrpt:String 利润分配 
        basiceps:float 每股收益(元) 
        bps:float 每股净资产 (元) 
        datatype:String 报告描述 
        datayear:String 报告年份 
        datemmdd:String 报告类型 
        mgjyxjje:float 每股经营现金流量(元) 
        noticedate:String 公告日期 
        parentnetprofit:float 净利润(元) 
        publishname:String 所处行业 
        qdate:String 季度 
        secucode:String 代码简写 
        securitytype:String 股票类型 
        sjlhz:float 净利润-季度环比增长(%) 
        sjltz:float 净利润-同比增长(%) 
        totaloperateincome:float 营业总收入(元) 
        trademarket:String 交易所 
        updatedate:String 最新公告日期 
        weightavgroe:float 净资产收益率(%) 
        xsmll:float 销售毛利率(%) 
        yshz:float 营业总收入-季度环比增长(%) 
        ystz:float 营业总收入-同比增长(%) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'assigndscrpt' : 'str', 'basiceps' : 'float', 'bps' : 'float', 'datatype' : 'str', 'datayear' : 'str', 'datemmdd' : 'str', 'mgjyxjje' : 'float', 'noticedate' : 'str', 'parentnetprofit' : 'float', 'publishname' : 'str', 'qdate' : 'str', 'secucode' : 'str', 'securitytype' : 'str', 'sjlhz' : 'float', 'sjltz' : 'float', 'totaloperateincome' : 'float', 'trademarket' : 'str', 'updatedate' : 'str', 'weightavgroe' : 'float', 'xsmll' : 'float', 'yshz' : 'float', 'ystz' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '报告日期', 'name': '股票名称', 'assigndscrpt': '利润分配', 'basiceps': '每股收益(元)', 'bps': '每股净资产 (元)', 'datatype': '报告描述', 'datayear': '报告年份', 'datemmdd': '报告类型', 'mgjyxjje': '每股经营现金流量(元)', 'noticedate': '公告日期', 'parentnetprofit': '净利润(元)', 'publishname': '所处行业', 'qdate': '季度', 'secucode': '代码简写', 'securitytype': '股票类型', 'sjlhz': '净利润-季度环比增长(%)', 'sjltz': '净利润-同比增长(%)', 'totaloperateincome': '营业总收入(元)', 'trademarket': '交易所', 'updatedate': '最新公告日期', 'weightavgroe': '净资产收益率(%)', 'xsmll': '销售毛利率(%)', 'yshz': '营业总收入-季度环比增长(%)', 'ystz': '营业总收入-同比增长(%)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getReportNianBao",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getReportKuaiBao(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        年报季报/业绩快报
        http://www.mushuju.com/b_performanceexpress.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 报告日期 
        name:String 股票名称 
        basiceps:float 每股收益(元) 
        datatype:String 报告描述 
        djdjlhz:float 净利润-季度环比增长(%) 
        djdyshz:float 营业总收入-季度环比增长(%) 
        jlrtbzcl:float 净利润-同比增长(%) 
        noticedate:String 公告日期 
        parentbvps:float 每股净资产(元) 
        parentnetprofit:float 净利润(元) 
        parentnetprofitsq:float 净利润-去年同期(元) 
        publishname:String 所处行业 
        secucode:String 代码简写 
        securitytype:String 股票类型 
        totaloperateincome:float 营业总收入(元) 
        totaloperateincomesq:float 营业总收入-去年同期(元) 
        trademarket:String 交易所 
        updatedate:String 最新公告日期 
        weightavgroe:float 净资产收益率 (%) 
        ystz:float 营业总收入-同比增长(%) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'basiceps' : 'float', 'datatype' : 'str', 'djdjlhz' : 'float', 'djdyshz' : 'float', 'jlrtbzcl' : 'float', 'noticedate' : 'str', 'parentbvps' : 'float', 'parentnetprofit' : 'float', 'parentnetprofitsq' : 'float', 'publishname' : 'str', 'secucode' : 'str', 'securitytype' : 'str', 'totaloperateincome' : 'float', 'totaloperateincomesq' : 'float', 'trademarket' : 'str', 'updatedate' : 'str', 'weightavgroe' : 'float', 'ystz' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '报告日期', 'name': '股票名称', 'basiceps': '每股收益(元)', 'datatype': '报告描述', 'djdjlhz': '净利润-季度环比增长(%)', 'djdyshz': '营业总收入-季度环比增长(%)', 'jlrtbzcl': '净利润-同比增长(%)', 'noticedate': '公告日期', 'parentbvps': '每股净资产(元)', 'parentnetprofit': '净利润(元)', 'parentnetprofitsq': '净利润-去年同期(元)', 'publishname': '所处行业', 'secucode': '代码简写', 'securitytype': '股票类型', 'totaloperateincome': '营业总收入(元)', 'totaloperateincomesq': '营业总收入-去年同期(元)', 'trademarket': '交易所', 'updatedate': '最新公告日期', 'weightavgroe': '净资产收益率 (%)', 'ystz': '营业总收入-同比增长(%)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getReportKuaiBao",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getReportYugao(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        年报季报/业绩预告
        http://www.mushuju.com/b_performanceforecast.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 报告日期 
        name:String 股票名称 
        addamplower:float 业绩变动幅度-下限（%） 
        addampupper:float 业绩变动幅度-上限（%） 
        changereasonexplain:String 业绩变动原因 
        noticedate:String 公告日期 
        predictamtlower:float 预测净利润-下限 
        predictamtupper:float 预测净利润-上限 
        predictcontent:String 业绩变动 
        predictfinance:String 预测指标 
        predicttype:String 预告类型 
        preyearsameperiod:float 上年同期值(元) 
        secucode:String 代码简写 
        securitytype:String 股票类型 
        trademarket:String 交易所 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'addamplower' : 'float', 'addampupper' : 'float', 'changereasonexplain' : 'str', 'noticedate' : 'str', 'predictamtlower' : 'float', 'predictamtupper' : 'float', 'predictcontent' : 'str', 'predictfinance' : 'str', 'predicttype' : 'str', 'preyearsameperiod' : 'float', 'secucode' : 'str', 'securitytype' : 'str', 'trademarket' : 'str'}
        column_name_dict = {'code': '股票代码', 'tdate': '报告日期', 'name': '股票名称', 'addamplower': '业绩变动幅度-下限（%）', 'addampupper': '业绩变动幅度-上限（%）', 'changereasonexplain': '业绩变动原因', 'noticedate': '公告日期', 'predictamtlower': '预测净利润-下限', 'predictamtupper': '预测净利润-上限', 'predictcontent': '业绩变动', 'predictfinance': '预测指标', 'predicttype': '预告类型', 'preyearsameperiod': '上年同期值(元)', 'secucode': '代码简写', 'securitytype': '股票类型', 'trademarket': '交易所'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getReportYugao",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getReportYuyueTime(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        年报季报/预约披露时间
        http://www.mushuju.com/b_disclosuretime.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 报告日期 
        name:String 股票名称 
        actualpublishdate:String 实际披露时间 
        appointpublishdate:String 预约披露时间 
        firstappointdate:String 首次预约时间 
        firstchangedate:String 一次变更日期 
        ispublish:int 是否披露 
        reporttype:String 报告类型代码 
        reporttypename:String 报告类型名称 
        reportyear:String 报告年份 
        secondchangedate:String 二次变更日期 
        secucode:String 代码简写 
        thirdchangedate:String 三次变更日期 
        trademarket:String 交易所 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'actualpublishdate' : 'str', 'appointpublishdate' : 'str', 'firstappointdate' : 'str', 'firstchangedate' : 'str', 'ispublish' : 'int', 'reporttype' : 'str', 'reporttypename' : 'str', 'reportyear' : 'str', 'secondchangedate' : 'str', 'secucode' : 'str', 'thirdchangedate' : 'str', 'trademarket' : 'str'}
        column_name_dict = {'code': '股票代码', 'tdate': '报告日期', 'name': '股票名称', 'actualpublishdate': '实际披露时间', 'appointpublishdate': '预约披露时间', 'firstappointdate': '首次预约时间', 'firstchangedate': '一次变更日期', 'ispublish': '是否披露', 'reporttype': '报告类型代码', 'reporttypename': '报告类型名称', 'reportyear': '报告年份', 'secondchangedate': '二次变更日期', 'secucode': '代码简写', 'thirdchangedate': '三次变更日期', 'trademarket': '交易所'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getReportYuyueTime",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getReportFuzhai(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        年报季报/资产负债表
        http://www.mushuju.com/b_balancesheet.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 报告日期 
        name:String 股票名称 
        acceptdeposit:float 吸收存款(元) 
        acceptdepositratio:float 吸收存款同比(%) 
        accountspayable:float 应付账款(元) 
        accountspayableratio:float 应付账款同比(%) 
        accountsrece:float 应收账款(元) 
        accountsreceratio:float 应收账款同比(%) 
        advancepremium:float 预收保费(元) 
        advancepremiumratio:float 预收保费同比(%) 
        advancereceivables:float 预收账款(元) 
        advancereceivablesratio:float 预收账款同比(%) 
        agenttradesecurity:float 代理买卖证券款(元) 
        availablesalefinasset:float 可供出售金融资产(元) 
        cashdepositpbc:float 存放中央银行款项(元) 
        debtassetratio:float 资产负债同比(%) 
        industryname:String 行业名称 
        inventory:float 存货(元) 
        inventoryratio:float 存货同比(%) 
        loanadvance:float 发放贷款及垫款(元) 
        loanadvanceratio:float 发放贷款及垫款同比(%) 
        loanpbc:float 向中央银行借款(元) 
        loanpbcratio:float 向中央银行借款同比(%) 
        monetaryfunds:float 货币资金(元) 
        monetaryfundsratio:float 货币资金同比(%) 
        noticedate:String 公告日期 
        premiumrece:float 应收保费(元) 
        premiumreceratio:float 应收保费同比(%) 
        reporttypecode:String 报告类型代码 
        secucode:String 代码简写 
        sellrepofinasset:float 卖出回购金融资产款(元) 
        settleexcessreserve:float 结算备付金(元) 
        shortloan:float 短期借款(元) 
        shortloanratio:float 短期借款同比(%) 
        totalassets:float 总资产(元) 
        totalassetsratio:float 总资产同比(%) 
        totalequity:float 股东权益合计(元) 
        totalliabilities:float 总负债(元) 
        totalliabratio:float 总负债同比(%) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'acceptdeposit' : 'float', 'acceptdepositratio' : 'float', 'accountspayable' : 'float', 'accountspayableratio' : 'float', 'accountsrece' : 'float', 'accountsreceratio' : 'float', 'advancepremium' : 'float', 'advancepremiumratio' : 'float', 'advancereceivables' : 'float', 'advancereceivablesratio' : 'float', 'agenttradesecurity' : 'float', 'availablesalefinasset' : 'float', 'cashdepositpbc' : 'float', 'debtassetratio' : 'float', 'industryname' : 'str', 'inventory' : 'float', 'inventoryratio' : 'float', 'loanadvance' : 'float', 'loanadvanceratio' : 'float', 'loanpbc' : 'float', 'loanpbcratio' : 'float', 'monetaryfunds' : 'float', 'monetaryfundsratio' : 'float', 'noticedate' : 'str', 'premiumrece' : 'float', 'premiumreceratio' : 'float', 'reporttypecode' : 'str', 'secucode' : 'str', 'sellrepofinasset' : 'float', 'settleexcessreserve' : 'float', 'shortloan' : 'float', 'shortloanratio' : 'float', 'totalassets' : 'float', 'totalassetsratio' : 'float', 'totalequity' : 'float', 'totalliabilities' : 'float', 'totalliabratio' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '报告日期', 'name': '股票名称', 'acceptdeposit': '吸收存款(元)', 'acceptdepositratio': '吸收存款同比(%)', 'accountspayable': '应付账款(元)', 'accountspayableratio': '应付账款同比(%)', 'accountsrece': '应收账款(元)', 'accountsreceratio': '应收账款同比(%)', 'advancepremium': '预收保费(元)', 'advancepremiumratio': '预收保费同比(%)', 'advancereceivables': '预收账款(元)', 'advancereceivablesratio': '预收账款同比(%)', 'agenttradesecurity': '代理买卖证券款(元)', 'availablesalefinasset': '可供出售金融资产(元)', 'cashdepositpbc': '存放中央银行款项(元)', 'debtassetratio': '资产负债同比(%)', 'industryname': '行业名称', 'inventory': '存货(元)', 'inventoryratio': '存货同比(%)', 'loanadvance': '发放贷款及垫款(元)', 'loanadvanceratio': '发放贷款及垫款同比(%)', 'loanpbc': '向中央银行借款(元)', 'loanpbcratio': '向中央银行借款同比(%)', 'monetaryfunds': '货币资金(元)', 'monetaryfundsratio': '货币资金同比(%)', 'noticedate': '公告日期', 'premiumrece': '应收保费(元)', 'premiumreceratio': '应收保费同比(%)', 'reporttypecode': '报告类型代码', 'secucode': '代码简写', 'sellrepofinasset': '卖出回购金融资产款(元)', 'settleexcessreserve': '结算备付金(元)', 'shortloan': '短期借款(元)', 'shortloanratio': '短期借款同比(%)', 'totalassets': '总资产(元)', 'totalassetsratio': '总资产同比(%)', 'totalequity': '股东权益合计(元)', 'totalliabilities': '总负债(元)', 'totalliabratio': '总负债同比(%)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getReportFuzhai",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getReportLirun(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        年报季报/利润表
        http://www.mushuju.com/b_profitstatement.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        tdate:String 报告日期 
        earnedpremium:float 已赚保费(元) 
        feecommissionni:float 手续费及佣金净收入(元) 
        financeexpense:float 财务费用(元) 
        industrycode:String 行业代码 
        industryname:String 行业名称 
        interestni:float 利息净收入(元) 
        investincome:float 投资收益(元) 
        manageexpense:float 管理费用(元) 
        noticedate:String 公告日期 
        operatecost:float 营业支出(元) 
        operateexpense:float 营业总支出(元) 
        operateexpenseratio:float 营业总支出同比(%) 
        operateincome:float 营业总收入(元) 
        operateprofit:float 营业利润(元) 
        operateprofitratio:float 营业利润同比(%) 
        operatetaxadd:float 营业税金及附加(元) 
        parentnetprofit:float 净利润(元) 
        parentnetprofitratio:float 净利润同比(%) 
        saleexpense:float 销售费用(元) 
        secucode:String 代码简写 
        toiratio:float 营业总收入同比(%) 
        totaloperatecost:float 营业总支出(元) 
        totaloperateincome:float 营业总收入(元) 
        totalprofit:float 利润总额(元) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'tdate' : 'str', 'earnedpremium' : 'float', 'feecommissionni' : 'float', 'financeexpense' : 'float', 'industrycode' : 'str', 'industryname' : 'str', 'interestni' : 'float', 'investincome' : 'float', 'manageexpense' : 'float', 'noticedate' : 'str', 'operatecost' : 'float', 'operateexpense' : 'float', 'operateexpenseratio' : 'float', 'operateincome' : 'float', 'operateprofit' : 'float', 'operateprofitratio' : 'float', 'operatetaxadd' : 'float', 'parentnetprofit' : 'float', 'parentnetprofitratio' : 'float', 'saleexpense' : 'float', 'secucode' : 'str', 'toiratio' : 'float', 'totaloperatecost' : 'float', 'totaloperateincome' : 'float', 'totalprofit' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'tdate': '报告日期', 'earnedpremium': '已赚保费(元)', 'feecommissionni': '手续费及佣金净收入(元)', 'financeexpense': '财务费用(元)', 'industrycode': '行业代码', 'industryname': '行业名称', 'interestni': '利息净收入(元)', 'investincome': '投资收益(元)', 'manageexpense': '管理费用(元)', 'noticedate': '公告日期', 'operatecost': '营业支出(元)', 'operateexpense': '营业总支出(元)', 'operateexpenseratio': '营业总支出同比(%)', 'operateincome': '营业总收入(元)', 'operateprofit': '营业利润(元)', 'operateprofitratio': '营业利润同比(%)', 'operatetaxadd': '营业税金及附加(元)', 'parentnetprofit': '净利润(元)', 'parentnetprofitratio': '净利润同比(%)', 'saleexpense': '销售费用(元)', 'secucode': '代码简写', 'toiratio': '营业总收入同比(%)', 'totaloperatecost': '营业总支出(元)', 'totaloperateincome': '营业总收入(元)', 'totalprofit': '利润总额(元)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getReportLirun",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getReportXianjin(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        年报季报/现金流量表
        http://www.mushuju.com/b_cashflowstatement.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        tdate:String 报告日期 
        cceadd:float 净现金流(元) 
        cceaddratio:float 净现金流同比增长(%) 
        netcashfinance:float 融资性现金流量净额(元) 
        netcashfinanceratio:float 融资性净现金流占比(%) 
        netcashinvest:float 投资性现金流量净额(元) 
        netcashinvestratio:float 投资性净现金流占比(%) 
        netcashoperate:float 经营性现金流量净额(元) 
        netcashoperateratio:float 经营性净现金流占比(%) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'tdate' : 'str', 'cceadd' : 'float', 'cceaddratio' : 'float', 'netcashfinance' : 'float', 'netcashfinanceratio' : 'float', 'netcashinvest' : 'float', 'netcashinvestratio' : 'float', 'netcashoperate' : 'float', 'netcashoperateratio' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'tdate': '报告日期', 'cceadd': '净现金流(元)', 'cceaddratio': '净现金流同比增长(%)', 'netcashfinance': '融资性现金流量净额(元)', 'netcashfinanceratio': '融资性净现金流占比(%)', 'netcashinvest': '投资性现金流量净额(元)', 'netcashinvestratio': '投资性净现金流占比(%)', 'netcashoperate': '经营性现金流量净额(元)', 'netcashoperateratio': '经营性净现金流占比(%)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getReportXianjin",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getReportFenhong(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        年报季报/分红送配
        http://www.mushuju.com/b_dividenddistribution.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        tdate:String 报告日期 
        assignprogress:String 方案进度 
        basiceps:float 每股收益(元) 
        bd10closeadjchrate:float 股权登记日，前10日涨幅(%) 
        bonusitratio:float 送转股份-送转总比例 
        bonusratio:float 送转股份-送股比例 
        itratio:float 送转股份-转股比例 
        bvps:float 每股净资产(元) 
        d10closeadjchrate:float 预案公告日，后10日涨幅(%) 
        d30closeadjchrate:float 除权除息日，后30日涨幅(%) 
        equityrecorddate:String 股权登记日 
        exdividenddate:String 除权除息日 
        exdividenddays:int 已除权天数(天) 
        noticedate:String 最新公告日期 
        percapitalreserve:float 每股公积金(元) 
        perunassignprofit:float 每股未分配利润(元) 
        plannoticedate:String 预案公告日 
        pnpyoyratio:float 净利润同比增长(%) 
        pretaxbonusrmb:float 现金分红-比例 
        dividentratio:float 现金分红-股息率(%) 
        publishdate:String 公布日期 
        secucode:String 代码简写 
        totalshares:float 总股本(元) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'tdate' : 'str', 'assignprogress' : 'str', 'basiceps' : 'float', 'bd10closeadjchrate' : 'float', 'bonusitratio' : 'float', 'bonusratio' : 'float', 'itratio' : 'float', 'bvps' : 'float', 'd10closeadjchrate' : 'float', 'd30closeadjchrate' : 'float', 'equityrecorddate' : 'str', 'exdividenddate' : 'str', 'exdividenddays' : 'int', 'noticedate' : 'str', 'percapitalreserve' : 'float', 'perunassignprofit' : 'float', 'plannoticedate' : 'str', 'pnpyoyratio' : 'float', 'pretaxbonusrmb' : 'float', 'dividentratio' : 'float', 'publishdate' : 'str', 'secucode' : 'str', 'totalshares' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'tdate': '报告日期', 'assignprogress': '方案进度', 'basiceps': '每股收益(元)', 'bd10closeadjchrate': '股权登记日，前10日涨幅(%)', 'bonusitratio': '送转股份-送转总比例', 'bonusratio': '送转股份-送股比例', 'itratio': '送转股份-转股比例', 'bvps': '每股净资产(元)', 'd10closeadjchrate': '预案公告日，后10日涨幅(%)', 'd30closeadjchrate': '除权除息日，后30日涨幅(%)', 'equityrecorddate': '股权登记日', 'exdividenddate': '除权除息日', 'exdividenddays': '已除权天数(天)', 'noticedate': '最新公告日期', 'percapitalreserve': '每股公积金(元)', 'perunassignprofit': '每股未分配利润(元)', 'plannoticedate': '预案公告日', 'pnpyoyratio': '净利润同比增长(%)', 'pretaxbonusrmb': '现金分红-比例', 'dividentratio': '现金分红-股息率(%)', 'publishdate': '公布日期', 'secucode': '代码简写', 'totalshares': '总股本(元)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getReportFenhong",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getHSGTMoney(self,mtype: int, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        沪深港通/资金流向
        http://www.mushuju.com/b_hsgtmoneyflow.html
        <参数>
        mtype:int 沪深港通资金类型，取值范围：1|沪股通(港>沪)资金-北向；2|深股通(港>深)资金-北向；3|北向资金；4|港股通(沪>港)资金-南向；5|港股通(深>港)资金-南向；6|南向资金 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线；104|季线；106|年线 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        mtype:int 资金类别 
        name:String 资金名称 
        ktype:int K线类别 
        tdate:String 交易时间 
        jlr:float 净流入（万元） 
        zjye:float 资金余额（万元） 
        ljjlr:float 累计净流入（万元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'mtype' : 'int', 'name' : 'str', 'ktype' : 'int', 'tdate' : 'str', 'jlr' : 'float', 'zjye' : 'float', 'ljjlr' : 'float'}
        column_name_dict = {'mtype': '资金类别', 'name': '资金名称', 'ktype': 'K线类别', 'tdate': '交易时间', 'jlr': '净流入（万元）', 'zjye': '资金余额（万元）', 'ljjlr': '累计净流入（万元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getHSGTMoney",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getHSGTBlockRank(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        沪深港通/板块每日行情
        http://www.mushuju.com/b_hsgtdailymarketplate.html
        <参数>
        code:String 板块代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 板块代码 
        tdate:String 交易日期 
        name:String 板块名称 
        addboardratio:float 北向资金今日增持估计-占板块比 
        addhkratio:float 北向资金今日增持估计-占北向资金比 
        addmarketcap:float 北向资金今日增持估计-市值 
        addratio:float 北向资金今日增持估计-市值增幅 
        boardhkratio:float 北向资金今日持股-占北向资金比 
        hkboardratio:float 北向资金今日持股-占板块比 
        hkvalue:float 北向资金今日持股-市值 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'addboardratio' : 'float', 'addhkratio' : 'float', 'addmarketcap' : 'float', 'addratio' : 'float', 'boardhkratio' : 'float', 'hkboardratio' : 'float', 'hkvalue' : 'float'}
        column_name_dict = {'code': '板块代码', 'tdate': '交易日期', 'name': '板块名称', 'addboardratio': '北向资金今日增持估计-占板块比', 'addhkratio': '北向资金今日增持估计-占北向资金比', 'addmarketcap': '北向资金今日增持估计-市值', 'addratio': '北向资金今日增持估计-市值增幅', 'boardhkratio': '北向资金今日持股-占北向资金比', 'hkboardratio': '北向资金今日持股-占板块比', 'hkvalue': '北向资金今日持股-市值'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getHSGTBlockRank",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getHSGTStockRank(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        沪深港通/个股每日行情
        http://www.mushuju.com/b_hsgtdailyquotationsindividual.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        tdate:String 交易日期 
        addmarketcap:float 今日增持估计-市值（万） 
        addsharesamp:float 今日增持估计-市值增幅（%） 
        addsharesrepair:float 今日增持估计-股数（万） 
        freecapratiochg:float 今日增持估计-占流通股比（‰） 
        totalratiochg:float 今日增持估计-占总股本比（‰） 
        freesharesratio:float 今日持股-占流通股比（%） 
        holdmarketcap:float 今日持股-市值（万） 
        holdshares:float 今日持股-股数（万） 
        totalsharesratio:float 今日持股-占总股本比（%） 
        areacode:String 地域板块代码 
        changerate:float 今日涨跌幅 
        closeprice:float 今日收盘价 
        industrycode:String 所属板块 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'tdate' : 'str', 'addmarketcap' : 'float', 'addsharesamp' : 'float', 'addsharesrepair' : 'float', 'freecapratiochg' : 'float', 'totalratiochg' : 'float', 'freesharesratio' : 'float', 'holdmarketcap' : 'float', 'holdshares' : 'float', 'totalsharesratio' : 'float', 'areacode' : 'str', 'changerate' : 'float', 'closeprice' : 'float', 'industrycode' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'tdate': '交易日期', 'addmarketcap': '今日增持估计-市值（万）', 'addsharesamp': '今日增持估计-市值增幅（%）', 'addsharesrepair': '今日增持估计-股数（万）', 'freecapratiochg': '今日增持估计-占流通股比（‰）', 'totalratiochg': '今日增持估计-占总股本比（‰）', 'freesharesratio': '今日持股-占流通股比（%）', 'holdmarketcap': '今日持股-市值（万）', 'holdshares': '今日持股-股数（万）', 'totalsharesratio': '今日持股-占总股本比（%）', 'areacode': '地域板块代码', 'changerate': '今日涨跌幅', 'closeprice': '今日收盘价', 'industrycode': '所属板块'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getHSGTStockRank",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getHSGTHistory(self,mtype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        沪深港通/历史数据
        http://www.mushuju.com/b_hsgthistoricaldata.html
        <参数>
        mtype:int 市场类别，取值范围：1|沪股通(港>沪)；2|深股通(港>深)；3|港股通(沪>港)；4|港股通(深>港) Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        mtype:int 市场类别 
        tdate:String 交易日期 
        accumdealamt:float 历史累计净买额 
        buyamt:float 买入成交额 
        fundinflow:float 当日资金流入 
        indexchangerate:float 涨跌幅 
        indexcloseprice:float 上证指数 
        leadstocksname:String 领涨股 
        lschangerate:float 领涨股-涨跌幅 
        netdealamt:float 当日成交净买额 
        quotabalance:float 当日余额 
        sellamt:float 卖出成交额 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'mtype' : 'int', 'tdate' : 'str', 'accumdealamt' : 'float', 'buyamt' : 'float', 'fundinflow' : 'float', 'indexchangerate' : 'float', 'indexcloseprice' : 'float', 'leadstocksname' : 'str', 'lschangerate' : 'float', 'netdealamt' : 'float', 'quotabalance' : 'float', 'sellamt' : 'float'}
        column_name_dict = {'mtype': '市场类别', 'tdate': '交易日期', 'accumdealamt': '历史累计净买额', 'buyamt': '买入成交额', 'fundinflow': '当日资金流入', 'indexchangerate': '涨跌幅', 'indexcloseprice': '上证指数', 'leadstocksname': '领涨股', 'lschangerate': '领涨股-涨跌幅', 'netdealamt': '当日成交净买额', 'quotabalance': '当日余额', 'sellamt': '卖出成交额'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getHSGTHistory",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getHsgtStockTop10(self,code: str, mtype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        沪深港通/十大成交股
        http://www.mushuju.com/b_hsgttentradedshares.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        mtype:int 市场类别，取值范围：1|沪股通(港>沪)；2|港股通(沪>港)；3|深股通(港>深)；4|港股通(深>港) Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        mtype:int 市场类型，1：沪股通；2：港股通(沪>港)；3:深股通；4：港股通(深>港) 
        tdate:String 交易时间 
        mkcode:String 代码 
        rankId:int 排名 
        securityname:String 股票简称 
        closeprice:float 收盘价 
        changerate:float 涨跌幅 
        netbuyamt:float 净买额（元） 
        buyamt:float 买入金额（元） 
        sellamt:float 卖出金额（元） 
        dealamt:float 成交金额（元） 
        dealAmount:float 总交易金额（元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'mtype' : 'int', 'tdate' : 'str', 'mkcode' : 'str', 'rankId' : 'int', 'securityname' : 'str', 'closeprice' : 'float', 'changerate' : 'float', 'netbuyamt' : 'float', 'buyamt' : 'float', 'sellamt' : 'float', 'dealamt' : 'float', 'dealAmount' : 'float'}
        column_name_dict = {'code': '股票代码', 'mtype': '市场类型，1：沪股通；2：港股通(沪>港)；3:深股通；4：港股通(深>港)', 'tdate': '交易时间', 'mkcode': '代码', 'rankId': '排名', 'securityname': '股票简称', 'closeprice': '收盘价', 'changerate': '涨跌幅', 'netbuyamt': '净买额（元）', 'buyamt': '买入金额（元）', 'sellamt': '卖出金额（元）', 'dealamt': '成交金额（元）', 'dealAmount': '总交易金额（元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getHsgtStockTop10",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getPoolZT(self,startDate: str, endDate: str, fields:str='all')->Query:
        """
        涨停板行情/涨停股池
        http://www.mushuju.com/b_dailylimit.html
        <参数>
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        tdate:String 交易时间 
        amount:float 成交额（元） 
        code:String 股票代码 
        fbt:String 首次封板时间,个股第一次触及涨（跌）停的时间 
        fund:float 封板资金（元），以涨停价买入挂单的资金总量 
        hs:float 换手率（%） 
        hybk:String 所属行业 
        lbc:int 连板数，个股连续封板数量 
        lbt:String 最后封板时间,个股最后一次触及涨（跌）停的时间 
        ltsz:float 流通市值（元） 
        m:int 0:深交所，1：上交所 
        n:String 股票名称 
        p:float 最新价 
        tshare:float 总市值（元） 
        zbc:int 炸板次数，个股打开涨停板的次数 
        zdp:float 涨跌幅（%） 
        zttj:String 涨停统计，n/m代表m天中有n次涨停板 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'tdate' : 'str', 'amount' : 'float', 'code' : 'str', 'fbt' : 'str', 'fund' : 'float', 'hs' : 'float', 'hybk' : 'str', 'lbc' : 'int', 'lbt' : 'str', 'ltsz' : 'float', 'm' : 'int', 'n' : 'str', 'p' : 'float', 'tshare' : 'float', 'zbc' : 'int', 'zdp' : 'float', 'zttj' : 'str'}
        column_name_dict = {'tdate': '交易时间', 'amount': '成交额（元）', 'code': '股票代码', 'fbt': '首次封板时间,个股第一次触及涨（跌）停的时间', 'fund': '封板资金（元），以涨停价买入挂单的资金总量', 'hs': '换手率（%）', 'hybk': '所属行业', 'lbc': '连板数，个股连续封板数量', 'lbt': '最后封板时间,个股最后一次触及涨（跌）停的时间', 'ltsz': '流通市值（元）', 'm': '0:深交所，1：上交所', 'n': '股票名称', 'p': '最新价', 'tshare': '总市值（元）', 'zbc': '炸板次数，个股打开涨停板的次数', 'zdp': '涨跌幅（%）', 'zttj': '涨停统计，n/m代表m天中有n次涨停板'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getPoolZT",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getPoolQS(self,startDate: str, endDate: str, fields:str='all')->Query:
        """
        涨停板行情/强势股池
        http://www.mushuju.com/b_stronglimit.html
        <参数>
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        tdate:String 交易时间 
        amount:float 成交额（元） 
        code:String 股票代码 
        cc:int 入选理由，1：60日新高，2：近期多次涨停，3：60日新高且近期多次涨停 
        hs:float 换手率（%） 
        hybk:String 所属行业 
        lb:float 量比 
        ltsz:float 流通市值（元） 
        m:int 0:深交所，1：上交所 
        n:String 股票名称 
        nh:int 是否新高，0：否，1：新高 
        p:float 最新价 
        tshare:float 总市值（元） 
        zdp:float 涨跌幅（%） 
        zs:float 涨速，三分钟涨跌幅 
        ztp:float 涨停价 
        zttj:String 涨停统计，n/m代表m天中有n次涨停板 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'tdate' : 'str', 'amount' : 'float', 'code' : 'str', 'cc' : 'int', 'hs' : 'float', 'hybk' : 'str', 'lb' : 'float', 'ltsz' : 'float', 'm' : 'int', 'n' : 'str', 'nh' : 'int', 'p' : 'float', 'tshare' : 'float', 'zdp' : 'float', 'zs' : 'float', 'ztp' : 'float', 'zttj' : 'str'}
        column_name_dict = {'tdate': '交易时间', 'amount': '成交额（元）', 'code': '股票代码', 'cc': '入选理由，1：60日新高，2：近期多次涨停，3：60日新高且近期多次涨停', 'hs': '换手率（%）', 'hybk': '所属行业', 'lb': '量比', 'ltsz': '流通市值（元）', 'm': '0:深交所，1：上交所', 'n': '股票名称', 'nh': '是否新高，0：否，1：新高', 'p': '最新价', 'tshare': '总市值（元）', 'zdp': '涨跌幅（%）', 'zs': '涨速，三分钟涨跌幅', 'ztp': '涨停价', 'zttj': '涨停统计，n/m代表m天中有n次涨停板'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getPoolQS",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getPoolCX(self,code: str, fields:str='all')->Query:
        """
        涨停板行情/次新股池
        http://www.mushuju.com/b_secondnew.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        amount:float 成交额（元） 
        code:String 股票代码 
        hs:float 换手率（%） 
        hybk:String 所属行业 
        ipod:String 上市日期 
        ltsz:float 流通市值（元） 
        m:int 0:深交所，1：上交所 
        n:String 股票名称 
        nh:int 是否新高，0：否，1：新高 
        od:String 开板日期 
        ods:int 开板几日 
        p:float 最新价 
        tshare:float 总市值（元） 
        zdp:float 涨跌幅（%） 
        ztf:float 涨停价 
        zttj:String 涨停统计，n/m代表m天中有n次涨停板 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'amount' : 'float', 'code' : 'str', 'hs' : 'float', 'hybk' : 'str', 'ipod' : 'str', 'ltsz' : 'float', 'm' : 'int', 'n' : 'str', 'nh' : 'int', 'od' : 'str', 'ods' : 'int', 'p' : 'float', 'tshare' : 'float', 'zdp' : 'float', 'ztf' : 'float', 'zttj' : 'str'}
        column_name_dict = {'amount': '成交额（元）', 'code': '股票代码', 'hs': '换手率（%）', 'hybk': '所属行业', 'ipod': '上市日期', 'ltsz': '流通市值（元）', 'm': '0:深交所，1：上交所', 'n': '股票名称', 'nh': '是否新高，0：否，1：新高', 'od': '开板日期', 'ods': '开板几日', 'p': '最新价', 'tshare': '总市值（元）', 'zdp': '涨跌幅（%）', 'ztf': '涨停价', 'zttj': '涨停统计，n/m代表m天中有n次涨停板'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getPoolCX",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getPoolZB(self,startDate: str, endDate: str, fields:str='all')->Query:
        """
        涨停板行情/炸板股池
        http://www.mushuju.com/b_friedplate.html
        <参数>
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        tdate:String 交易时间 
        amount:float 成交额（元） 
        code:String 股票代码 
        fbt:String 首次封板时间，个股第一次触及涨（跌）停的时间 
        hs:float 换手率（%） 
        hybk:String 所属行业 
        ltsz:float 流通市值（元） 
        m:int 0:深交所，1：上交所 
        n:String 股票名称 
        p:float 最新价 
        tshare:float 总市值（元） 
        zbc:int 炸板次数，个股打开涨停板的次数 
        zdp:float 涨跌幅（%） 
        zf:float 振幅（%） 
        zs:float 涨速（三分钟涨跌幅） 
        ztp:float 涨停价 
        zttj:String 涨停统计，n/m代表m天中有n次涨停板 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'tdate' : 'str', 'amount' : 'float', 'code' : 'str', 'fbt' : 'str', 'hs' : 'float', 'hybk' : 'str', 'ltsz' : 'float', 'm' : 'int', 'n' : 'str', 'p' : 'float', 'tshare' : 'float', 'zbc' : 'int', 'zdp' : 'float', 'zf' : 'float', 'zs' : 'float', 'ztp' : 'float', 'zttj' : 'str'}
        column_name_dict = {'tdate': '交易时间', 'amount': '成交额（元）', 'code': '股票代码', 'fbt': '首次封板时间，个股第一次触及涨（跌）停的时间', 'hs': '换手率（%）', 'hybk': '所属行业', 'ltsz': '流通市值（元）', 'm': '0:深交所，1：上交所', 'n': '股票名称', 'p': '最新价', 'tshare': '总市值（元）', 'zbc': '炸板次数，个股打开涨停板的次数', 'zdp': '涨跌幅（%）', 'zf': '振幅（%）', 'zs': '涨速（三分钟涨跌幅）', 'ztp': '涨停价', 'zttj': '涨停统计，n/m代表m天中有n次涨停板'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getPoolZB",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getPoolDT(self,startDate: str, endDate: str, fields:str='all')->Query:
        """
        涨停板行情/跌停股池
        http://www.mushuju.com/b_droplimit.html
        <参数>
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        tdate:String 交易时间 
        amount:float 成交额（元） 
        code:String 股票代码 
        days:int 连续跌停 
        fba:float 板上成交额（元），成交价格为该股票跌停价的所有成交额的总和 
        fund:float 封单资金（元） 
        hs:float 换手率（%） 
        hybk:String 所属行业 
        lbt:String 最后封板时间,个股最后一次触及涨（跌）停的时间 
        ltsz:float 流通市值（元） 
        m:int 0:深交所，1：上交所 
        n:String 股票名称 
        oc:int 开板次数，个股打开跌停板的次数 
        p:float 最新价 
        pe:float 动态市盈率 
        tshare:float 总市值（元） 
        zdp:float 涨跌幅（%） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'tdate' : 'str', 'amount' : 'float', 'code' : 'str', 'days' : 'int', 'fba' : 'float', 'fund' : 'float', 'hs' : 'float', 'hybk' : 'str', 'lbt' : 'str', 'ltsz' : 'float', 'm' : 'int', 'n' : 'str', 'oc' : 'int', 'p' : 'float', 'pe' : 'float', 'tshare' : 'float', 'zdp' : 'float'}
        column_name_dict = {'tdate': '交易时间', 'amount': '成交额（元）', 'code': '股票代码', 'days': '连续跌停', 'fba': '板上成交额（元），成交价格为该股票跌停价的所有成交额的总和', 'fund': '封单资金（元）', 'hs': '换手率（%）', 'hybk': '所属行业', 'lbt': '最后封板时间,个股最后一次触及涨（跌）停的时间', 'ltsz': '流通市值（元）', 'm': '0:深交所，1：上交所', 'n': '股票名称', 'oc': '开板次数，个股打开跌停板的次数', 'p': '最新价', 'pe': '动态市盈率', 'tshare': '总市值（元）', 'zdp': '涨跌幅（%）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getPoolDT",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getFundBaseInfo(self,code: str, fields:str='all')->Query:
        """
        基金数据/基金列表
        http://www.mushuju.com/b_fundlist.html
        <参数>
        code:String 基金代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 基金代码 
        name:String 基金名称 
        jjlb:String 基金类别 
        clrq:String 基金成立日期 
        clgm:String 基金成立规模 
        zcgm:String 资产规模 
        fegm:String 份额规模 
        glr:String 管理人 
        tgr:String 托管人 
        jlr:String 经理人 
        lsfh:String 历史分红 
        glf:String 管理费 
        fgf:String 托管费 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'jjlb' : 'str', 'clrq' : 'str', 'clgm' : 'str', 'zcgm' : 'str', 'fegm' : 'str', 'glr' : 'str', 'tgr' : 'str', 'jlr' : 'str', 'lsfh' : 'str', 'glf' : 'str', 'fgf' : 'str'}
        column_name_dict = {'code': '基金代码', 'name': '基金名称', 'jjlb': '基金类别', 'clrq': '基金成立日期', 'clgm': '基金成立规模', 'zcgm': '资产规模', 'fegm': '份额规模', 'glr': '管理人', 'tgr': '托管人', 'jlr': '经理人', 'lsfh': '历史分红', 'glf': '管理费', 'fgf': '托管费'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getFundBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getFundRank(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基金数据/每日行情
        http://www.mushuju.com/b_funddailymarket.html
        <参数>
        code:String 基金代码，code参数可以从【基金->基金列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 基金代码 
        name:String 基金名称 
        jjjc:String 基金拼音名称 
        jjdl:int 基金大类，0：场内基金 1：场外基金 
        jjlb:String 基金类别，场外-股票型基金、场外-混合型基金、场外-债券型基金、场外-指数型基金、场外-QDII、场外-LOF、场外-FOF 
        cdate:String 基金成立日期 
        tdate:String 更新日期 
        nav:String 基金单位净值 
        ljjz:String 基金累计净值 
        rz:String 日增长率 
        yzz:String 近1周增长率 
        yyz:String 近1月增长率 
        syz:String 近3月增长率 
        lyz:String 近6月增长率 
        ynz:String 近1年增长率 
        lnz:String 近2年增长率 
        snz:String 近3年增长率 
        jnz:String 今年来增长率 
        clz:String 成立来增长率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'jjjc' : 'str', 'jjdl' : 'int', 'jjlb' : 'str', 'cdate' : 'str', 'tdate' : 'str', 'nav' : 'str', 'ljjz' : 'str', 'rz' : 'str', 'yzz' : 'str', 'yyz' : 'str', 'syz' : 'str', 'lyz' : 'str', 'ynz' : 'str', 'lnz' : 'str', 'snz' : 'str', 'jnz' : 'str', 'clz' : 'str'}
        column_name_dict = {'code': '基金代码', 'name': '基金名称', 'jjjc': '基金拼音名称', 'jjdl': '基金大类，0：场内基金 1：场外基金', 'jjlb': '基金类别，场外-股票型基金、场外-混合型基金、场外-债券型基金、场外-指数型基金、场外-QDII、场外-LOF、场外-FOF', 'cdate': '基金成立日期', 'tdate': '更新日期', 'nav': '基金单位净值', 'ljjz': '基金累计净值', 'rz': '日增长率', 'yzz': '近1周增长率', 'yyz': '近1月增长率', 'syz': '近3月增长率', 'lyz': '近6月增长率', 'ynz': '近1年增长率', 'lnz': '近2年增长率', 'snz': '近3年增长率', 'jnz': '今年来增长率', 'clz': '成立来增长率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getFundRank",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getFundNav(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基金数据/基金净值
        http://www.mushuju.com/b_netfundvalue.html
        <参数>
        code:String 基金代码，code参数可以从【基金->基金列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 基金代码 
        tdate:String 净值日期 
        nav:String 基金单位净值 
        ljjz:String 基金累计净值 
        rzgl:String 日增长率(%) 
        sgzt:String 申购状态 
        shzt:String 赎回状态 
        fhje:String 分红金额 
        fhms:String 分红描述 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'nav' : 'str', 'ljjz' : 'str', 'rzgl' : 'str', 'sgzt' : 'str', 'shzt' : 'str', 'fhje' : 'str', 'fhms' : 'str'}
        column_name_dict = {'code': '基金代码', 'tdate': '净值日期', 'nav': '基金单位净值', 'ljjz': '基金累计净值', 'rzgl': '日增长率(%)', 'sgzt': '申购状态', 'shzt': '赎回状态', 'fhje': '分红金额', 'fhms': '分红描述'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getFundNav",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getFundMaxBack(self,code: str, fields:str='all')->Query:
        """
        基金数据/最大回撤
        http://www.mushuju.com/b_maximumpullback.html
        <参数>
        code:String 基金代码，code参数可以从【基金->基金列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 基金代码 
        name:String 股票名称 
        maxBack:float 基金最大回撤率 
        maxNavDate:String 基金最大净值日期 
        minNavDate:String 基金最小净值日期 
        maxNavValue:float 基金累计净值最大值 
        minNavValue:float 基金累计净值最小值 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'maxBack' : 'float', 'maxNavDate' : 'str', 'minNavDate' : 'str', 'maxNavValue' : 'float', 'minNavValue' : 'float'}
        column_name_dict = {'code': '基金代码', 'name': '股票名称', 'maxBack': '基金最大回撤率', 'maxNavDate': '基金最大净值日期', 'minNavDate': '基金最小净值日期', 'maxNavValue': '基金累计净值最大值', 'minNavValue': '基金累计净值最小值'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getFundMaxBack",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getFundPosition(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基金数据/基金持仓
        http://www.mushuju.com/b_fundposition.html
        <参数>
        code:String 基金代码，code参数可以从【基金->基金列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 基金代码 
        name:String 基金名称 
        tdate:String 公布日期 
        scode:String 股票代码 
        sname:String 股票名称 
        zjzbl:String 占净值比例 
        cgs:String 持股数（万股） 
        ccsz:String 持仓市值（万元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'tdate' : 'str', 'scode' : 'str', 'sname' : 'str', 'zjzbl' : 'str', 'cgs' : 'str', 'ccsz' : 'str'}
        column_name_dict = {'code': '基金代码', 'name': '基金名称', 'tdate': '公布日期', 'scode': '股票代码', 'sname': '股票名称', 'zjzbl': '占净值比例', 'cgs': '持股数（万股）', 'ccsz': '持仓市值（万元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getFundPosition",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockPosition(self,scode: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        基金数据/股票持仓
        http://www.mushuju.com/b_stockposition.html
        <参数>
        scode:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 基金代码 
        name:String 基金名称 
        tdate:String 公布日期 
        scode:String 股票代码 
        sname:String 股票名称 
        zjzbl:String 占净值比例 
        cgs:String 持股数（万股） 
        ccsz:String 持仓市值（万元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'tdate' : 'str', 'scode' : 'str', 'sname' : 'str', 'zjzbl' : 'str', 'cgs' : 'str', 'ccsz' : 'str'}
        column_name_dict = {'code': '基金代码', 'name': '基金名称', 'tdate': '公布日期', 'scode': '股票代码', 'sname': '股票名称', 'zjzbl': '占净值比例', 'cgs': '持股数（万股）', 'ccsz': '持仓市值（万元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockPosition",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getCnFundBaseInfo(self,code: str, fields:str='all')->Query:
        """
        基金数据/场内基金
        http://www.mushuju.com/b_fundbaseInfo.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getCnFundBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getCnFundDailyMarket(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        场内基金/每日行情
        http://www.mushuju.com/b_fundbaseInfodailymarket.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 股票名称 
        price:float 最新价（元） 
        zdfd:float 涨跌幅度（%） 
        zded:float 涨跌额度（元） 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        zhfu:float 振幅（%） 
        hslv:float 换手率（%） 
        sjlv:float 市净率（%） 
        dsyl:float 市盈率（动态） 
        jsyl:float 市盈率(静) 
        ttmsyl:float 市盈率(TTM) 
        ztj:float 涨停价（元） 
        dtj:float 跌停价（元） 
        jjia:float 均价（元） 
        lbi:float 量比 
        zgj:float 最高价（元） 
        zdj:float 最低价（元） 
        jrkpj:float 今日开盘价（元） 
        zrspj:float 昨日收盘价（元） 
        ssdate:String 上市日期 
        weibi:float 委比（%） 
        wpan:float 外盘（手） 
        npan:float 内盘（手） 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        mgsy:float 每股收益（元） 
        zf05:float 5日涨幅（%） 
        zf10:float 10日涨幅（%） 
        zf20:float 20日涨幅（%） 
        zf60:float 60日涨幅（%） 
        zfy:float 今年以来涨幅（%） 
        zys:float 总营收（元） 
        zystb:float 总营收同比（%） 
        jzc:float 净资产 
        jlr:float 净利润 
        mlil:float 毛利率 
        jlil:float 净利率 
        fzl:float 负债率 
        mgwfplr:float 每股未分配利润 
        mgjzc:float 每股净资产 
        mggjj:float 每股公积金（元） 
        zljlr:float 今日主力净流入（元） 
        cddlr:float 今日超大单流入（元） 
        cddlc:float 今日超大单流出（元） 
        cddjlr:float 今日超大单净流入（元） 
        ddlr:float 今日大单流入（元） 
        ddlc:float 今日大单流出（元） 
        ddjlr:float 今日大单净流入（元） 
        zdlr:float 今日中单流入（元） 
        zdlc:float 今日中单流出（元） 
        zdjlr:float 今日中单净流入（元） 
        xdlr:float 今日小单流入（元） 
        xdlc:float 今日小单流出（元） 
        xdjlr:float 今日小单净流入（元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'price' : 'float', 'zdfd' : 'float', 'zded' : 'float', 'cjl' : 'float', 'cje' : 'float', 'zhfu' : 'float', 'hslv' : 'float', 'sjlv' : 'float', 'dsyl' : 'float', 'jsyl' : 'float', 'ttmsyl' : 'float', 'ztj' : 'float', 'dtj' : 'float', 'jjia' : 'float', 'lbi' : 'float', 'zgj' : 'float', 'zdj' : 'float', 'jrkpj' : 'float', 'zrspj' : 'float', 'ssdate' : 'str', 'weibi' : 'float', 'wpan' : 'float', 'npan' : 'float', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'mgsy' : 'float', 'zf05' : 'float', 'zf10' : 'float', 'zf20' : 'float', 'zf60' : 'float', 'zfy' : 'float', 'zys' : 'float', 'zystb' : 'float', 'jzc' : 'float', 'jlr' : 'float', 'mlil' : 'float', 'jlil' : 'float', 'fzl' : 'float', 'mgwfplr' : 'float', 'mgjzc' : 'float', 'mggjj' : 'float', 'zljlr' : 'float', 'cddlr' : 'float', 'cddlc' : 'float', 'cddjlr' : 'float', 'ddlr' : 'float', 'ddlc' : 'float', 'ddjlr' : 'float', 'zdlr' : 'float', 'zdlc' : 'float', 'zdjlr' : 'float', 'xdlr' : 'float', 'xdlc' : 'float', 'xdjlr' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '股票名称', 'price': '最新价（元）', 'zdfd': '涨跌幅度（%）', 'zded': '涨跌额度（元）', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'zhfu': '振幅（%）', 'hslv': '换手率（%）', 'sjlv': '市净率（%）', 'dsyl': '市盈率（动态）', 'jsyl': '市盈率(静)', 'ttmsyl': '市盈率(TTM)', 'ztj': '涨停价（元）', 'dtj': '跌停价（元）', 'jjia': '均价（元）', 'lbi': '量比', 'zgj': '最高价（元）', 'zdj': '最低价（元）', 'jrkpj': '今日开盘价（元）', 'zrspj': '昨日收盘价（元）', 'ssdate': '上市日期', 'weibi': '委比（%）', 'wpan': '外盘（手）', 'npan': '内盘（手）', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'mgsy': '每股收益（元）', 'zf05': '5日涨幅（%）', 'zf10': '10日涨幅（%）', 'zf20': '20日涨幅（%）', 'zf60': '60日涨幅（%）', 'zfy': '今年以来涨幅（%）', 'zys': '总营收（元）', 'zystb': '总营收同比（%）', 'jzc': '净资产', 'jlr': '净利润', 'mlil': '毛利率', 'jlil': '净利率', 'fzl': '负债率', 'mgwfplr': '每股未分配利润', 'mgjzc': '每股净资产', 'mggjj': '每股公积金（元）', 'zljlr': '今日主力净流入（元）', 'cddlr': '今日超大单流入（元）', 'cddlc': '今日超大单流出（元）', 'cddjlr': '今日超大单净流入（元）', 'ddlr': '今日大单流入（元）', 'ddlc': '今日大单流出（元）', 'ddjlr': '今日大单净流入（元）', 'zdlr': '今日中单流入（元）', 'zdlc': '今日中单流出（元）', 'zdjlr': '今日中单净流入（元）', 'xdlr': '今日小单流入（元）', 'xdlc': '今日小单流出（元）', 'xdjlr': '今日小单净流入（元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getCnFundDailyMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getCnFundMinuteKLine(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        场内基金/分线数据
        http://www.mushuju.com/b_fundbranchdata.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 分时时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        cjjj:float 成交均价 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'cjjj' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '分时时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'cjjj': '成交均价'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getCnFundMinuteKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getCnFundHourKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        场内基金/时线数据
        http://www.mushuju.com/b_fundtimelinedata.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        ktype:int K线类别，取值范围：5|5分钟；15|15分钟；30|30分钟；60|60分钟 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getCnFundHourKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getCnFundADayKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        场内基金/日线数据
        http://www.mushuju.com/b_funddailydata.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getCnFundADayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHyBKBaseInfo(self,code: str, fields:str='all')->Query:
        """
        行业数据/行业板块
        http://www.mushuju.com/b_industrysector.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHyBKBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockGnBKBaseInfo(self,code: str, fields:str='all')->Query:
        """
        行业数据/概念板块
        http://www.mushuju.com/b_conceptblock.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockGnBKBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockDyBKBaseInfo(self,code: str, fields:str='all')->Query:
        """
        行业数据/地域板块
        http://www.mushuju.com/b_regionalplate.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockDyBKBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHYADailyMarket(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        行业数据/每日行情
        http://www.mushuju.com/b_industrydailymarket.html
        <参数>
        code:String 板块代码（行业板块、地域板块、概念板块），code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 股票名称 
        price:float 最新价（元） 
        zdfd:float 涨跌幅度（%） 
        zded:float 涨跌额度（元） 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        zhfu:float 振幅（%） 
        hslv:float 换手率（%） 
        sjlv:float 市净率（%） 
        dsyl:float 市盈率（动态） 
        jsyl:float 市盈率(静) 
        ttmsyl:float 市盈率(TTM) 
        ztj:float 涨停价（元） 
        dtj:float 跌停价（元） 
        jjia:float 均价（元） 
        lbi:float 量比 
        zgj:float 最高价（元） 
        zdj:float 最低价（元） 
        jrkpj:float 今日开盘价（元） 
        zrspj:float 昨日收盘价（元） 
        ssdate:String 上市日期 
        weibi:float 委比（%） 
        wpan:float 外盘（手） 
        npan:float 内盘（手） 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        mgsy:float 每股收益（元） 
        zf05:float 5日涨幅（%） 
        zf10:float 10日涨幅（%） 
        zf20:float 20日涨幅（%） 
        zf60:float 60日涨幅（%） 
        zfy:float 今年以来涨幅（%） 
        zys:float 总营收（元） 
        zystb:float 总营收同比（%） 
        jzc:float 净资产 
        jlr:float 净利润 
        mlil:float 毛利率 
        jlil:float 净利率 
        fzl:float 负债率 
        mgwfplr:float 每股未分配利润 
        mgjzc:float 每股净资产 
        mggjj:float 每股公积金（元） 
        zljlr:float 今日主力净流入（元） 
        cddlr:float 今日超大单流入（元） 
        cddlc:float 今日超大单流出（元） 
        cddjlr:float 今日超大单净流入（元） 
        ddlr:float 今日大单流入（元） 
        ddlc:float 今日大单流出（元） 
        ddjlr:float 今日大单净流入（元） 
        zdlr:float 今日中单流入（元） 
        zdlc:float 今日中单流出（元） 
        zdjlr:float 今日中单净流入（元） 
        xdlr:float 今日小单流入（元） 
        xdlc:float 今日小单流出（元） 
        xdjlr:float 今日小单净流入（元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'price' : 'float', 'zdfd' : 'float', 'zded' : 'float', 'cjl' : 'float', 'cje' : 'float', 'zhfu' : 'float', 'hslv' : 'float', 'sjlv' : 'float', 'dsyl' : 'float', 'jsyl' : 'float', 'ttmsyl' : 'float', 'ztj' : 'float', 'dtj' : 'float', 'jjia' : 'float', 'lbi' : 'float', 'zgj' : 'float', 'zdj' : 'float', 'jrkpj' : 'float', 'zrspj' : 'float', 'ssdate' : 'str', 'weibi' : 'float', 'wpan' : 'float', 'npan' : 'float', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'mgsy' : 'float', 'zf05' : 'float', 'zf10' : 'float', 'zf20' : 'float', 'zf60' : 'float', 'zfy' : 'float', 'zys' : 'float', 'zystb' : 'float', 'jzc' : 'float', 'jlr' : 'float', 'mlil' : 'float', 'jlil' : 'float', 'fzl' : 'float', 'mgwfplr' : 'float', 'mgjzc' : 'float', 'mggjj' : 'float', 'zljlr' : 'float', 'cddlr' : 'float', 'cddlc' : 'float', 'cddjlr' : 'float', 'ddlr' : 'float', 'ddlc' : 'float', 'ddjlr' : 'float', 'zdlr' : 'float', 'zdlc' : 'float', 'zdjlr' : 'float', 'xdlr' : 'float', 'xdlc' : 'float', 'xdjlr' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '股票名称', 'price': '最新价（元）', 'zdfd': '涨跌幅度（%）', 'zded': '涨跌额度（元）', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'zhfu': '振幅（%）', 'hslv': '换手率（%）', 'sjlv': '市净率（%）', 'dsyl': '市盈率（动态）', 'jsyl': '市盈率(静)', 'ttmsyl': '市盈率(TTM)', 'ztj': '涨停价（元）', 'dtj': '跌停价（元）', 'jjia': '均价（元）', 'lbi': '量比', 'zgj': '最高价（元）', 'zdj': '最低价（元）', 'jrkpj': '今日开盘价（元）', 'zrspj': '昨日收盘价（元）', 'ssdate': '上市日期', 'weibi': '委比（%）', 'wpan': '外盘（手）', 'npan': '内盘（手）', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'mgsy': '每股收益（元）', 'zf05': '5日涨幅（%）', 'zf10': '10日涨幅（%）', 'zf20': '20日涨幅（%）', 'zf60': '60日涨幅（%）', 'zfy': '今年以来涨幅（%）', 'zys': '总营收（元）', 'zystb': '总营收同比（%）', 'jzc': '净资产', 'jlr': '净利润', 'mlil': '毛利率', 'jlil': '净利率', 'fzl': '负债率', 'mgwfplr': '每股未分配利润', 'mgjzc': '每股净资产', 'mggjj': '每股公积金（元）', 'zljlr': '今日主力净流入（元）', 'cddlr': '今日超大单流入（元）', 'cddlc': '今日超大单流出（元）', 'cddjlr': '今日超大单净流入（元）', 'ddlr': '今日大单流入（元）', 'ddlc': '今日大单流出（元）', 'ddjlr': '今日大单净流入（元）', 'zdlr': '今日中单流入（元）', 'zdlc': '今日中单流出（元）', 'zdjlr': '今日中单净流入（元）', 'xdlr': '今日小单流入（元）', 'xdlc': '今日小单流出（元）', 'xdjlr': '今日小单净流入（元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHYADailyMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockBKDayKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        行业数据/日线数据
        http://www.mushuju.com/b_industrydailydata.html
        <参数>
        code:String 板块代码（行业板块、地域板块、概念板块），code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockBKDayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getGZMarket(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        估值数据/市场估值
        http://www.mushuju.com/b_marketvaluation.html
        <参数>
        code:String 市场代码，取值范围：000300|沪深两市；000001|沪市主板；000688|科创板；399001|深市主板；399006|创业板 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 市场代码 
        tdate:String 交易日期 
        pe_ttm_avg:float 平均市盈率 
        total_shares:float 总股本(股) 
        free_shares:float 流通股本(股) 
        total_market_cap:float 总市值(元) 
        free_market_cap:float 流通市值(元) 
        listing_org_num:float 个股总数 
        close_price:float 指数 
        change_rate:float 涨跌幅(%) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'pe_ttm_avg' : 'float', 'total_shares' : 'float', 'free_shares' : 'float', 'total_market_cap' : 'float', 'free_market_cap' : 'float', 'listing_org_num' : 'float', 'close_price' : 'float', 'change_rate' : 'float'}
        column_name_dict = {'code': '市场代码', 'tdate': '交易日期', 'pe_ttm_avg': '平均市盈率', 'total_shares': '总股本(股)', 'free_shares': '流通股本(股)', 'total_market_cap': '总市值(元)', 'free_market_cap': '流通市值(元)', 'listing_org_num': '个股总数', 'close_price': '指数', 'change_rate': '涨跌幅(%)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getGZMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getGZHangYe(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        估值数据/行业估值
        http://www.mushuju.com/b_industryvaluation.html
        <参数>
        code:String 板块代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 板块代码 
        name:String 市场名称 
        tdate:String 交易时间 
        pe_ttm:float PE(TTM)：当前股价与前四季度每股收益总和的比值 
        pe_lar:float PE(静)：当前股价与最新年报每股收益的比值 
        pb_mrq:float 市净率 
        peg_car:float PEG值：PE(TTM)与每股收益近3年复合增长率的比值 
        pcf_ocf_ttm:float 市现率(TTM)：当前股价与前四季度每股现金流量总和的比值 
        ps_ttm:float 市销率(TTM)：总市值与前四季度营业收入总和的比值 
        market_cap_vag:float 平均市值 (亿元) 
        num:float 个股数量 
        loss_count:float 亏损家数 
        free_shares_vag:float 平均流通股本(股) 
        nomarketcap_a_vag:float 平均流通市值(元) 
        notlimited_marketcap_a:float 流通市值(元) 
        total_market_cap:float 总市值(元) 
        total_shares:float 总股本(股) 
        total_shares_vag:float 平均股本(股) 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'tdate' : 'str', 'pe_ttm' : 'float', 'pe_lar' : 'float', 'pb_mrq' : 'float', 'peg_car' : 'float', 'pcf_ocf_ttm' : 'float', 'ps_ttm' : 'float', 'market_cap_vag' : 'float', 'num' : 'float', 'loss_count' : 'float', 'free_shares_vag' : 'float', 'nomarketcap_a_vag' : 'float', 'notlimited_marketcap_a' : 'float', 'total_market_cap' : 'float', 'total_shares' : 'float', 'total_shares_vag' : 'float'}
        column_name_dict = {'code': '板块代码', 'name': '市场名称', 'tdate': '交易时间', 'pe_ttm': 'PE(TTM)：当前股价与前四季度每股收益总和的比值', 'pe_lar': 'PE(静)：当前股价与最新年报每股收益的比值', 'pb_mrq': '市净率', 'peg_car': 'PEG值：PE(TTM)与每股收益近3年复合增长率的比值', 'pcf_ocf_ttm': '市现率(TTM)：当前股价与前四季度每股现金流量总和的比值', 'ps_ttm': '市销率(TTM)：总市值与前四季度营业收入总和的比值', 'market_cap_vag': '平均市值 (亿元)', 'num': '个股数量', 'loss_count': '亏损家数', 'free_shares_vag': '平均流通股本(股)', 'nomarketcap_a_vag': '平均流通市值(元)', 'notlimited_marketcap_a': '流通市值(元)', 'total_market_cap': '总市值(元)', 'total_shares': '总股本(股)', 'total_shares_vag': '平均股本(股)'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getGZHangYe",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getHSGZStock(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        估值数据/个股估值
        http://www.mushuju.com/b_individualstockvaluation.html
        <参数>
        code:String 股票代码，code参数可以从【沪深京股票->A股->A股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 市场名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        tdate:String 交易时间 
        pe_ttm:float PE(TTM)：当前股价与前四季度每股收益总和的比值 
        pe_lar:float PE(静)：当前股价与最新年报每股收益的比值 
        pb_mrq:float 市净率 
        peg_car:float PEG值：PE(TTM)与每股收益近3年复合增长率的比值 
        pcf_ocf_ttm:float 市现率(TTM)：当前股价与前四季度每股现金流量总和的比值 
        ps_ttm:float 市销率(TTM)：总市值与前四季度营业收入总和的比值 
        total_market_cap:float 总市值(元) 
        total_shares:float 总股本(股) 
        board_code:String 内部代码 
        board_name:String 板块名称 
        change_rate:float 涨跌幅(%) 
        close_price:float 收盘价 
        notlimited_marketcap_a:float 流通市值(元) 
        orig_board_code:String 板块代码 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'tdate' : 'str', 'pe_ttm' : 'float', 'pe_lar' : 'float', 'pb_mrq' : 'float', 'peg_car' : 'float', 'pcf_ocf_ttm' : 'float', 'ps_ttm' : 'float', 'total_market_cap' : 'float', 'total_shares' : 'float', 'board_code' : 'str', 'board_name' : 'str', 'change_rate' : 'float', 'close_price' : 'float', 'notlimited_marketcap_a' : 'float', 'orig_board_code' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '市场名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'tdate': '交易时间', 'pe_ttm': 'PE(TTM)：当前股价与前四季度每股收益总和的比值', 'pe_lar': 'PE(静)：当前股价与最新年报每股收益的比值', 'pb_mrq': '市净率', 'peg_car': 'PEG值：PE(TTM)与每股收益近3年复合增长率的比值', 'pcf_ocf_ttm': '市现率(TTM)：当前股价与前四季度每股现金流量总和的比值', 'ps_ttm': '市销率(TTM)：总市值与前四季度营业收入总和的比值', 'total_market_cap': '总市值(元)', 'total_shares': '总股本(股)', 'board_code': '内部代码', 'board_name': '板块名称', 'change_rate': '涨跌幅(%)', 'close_price': '收盘价', 'notlimited_marketcap_a': '流通市值(元)', 'orig_board_code': '板块代码'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getHSGZStock",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHKBaseInfo(self,code: str, fields:str='all')->Query:
        """
        港股数据/港股列表
        http://www.mushuju.com/b_hkstockslist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHKBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHKDailyMarket(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        港股数据/每日行情
        http://www.mushuju.com/b_hkdailymarket.html
        <参数>
        code:String 股票代码，code参数可以从【港股->港股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 股票名称 
        price:float 最新价（元） 
        zdfd:float 涨跌幅度（%） 
        zded:float 涨跌额度（元） 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        zhfu:float 振幅（%） 
        hslv:float 换手率（%） 
        sjlv:float 市净率（%） 
        dsyl:float 市盈率（动态） 
        jsyl:float 市盈率(静) 
        ttmsyl:float 市盈率(TTM) 
        ztj:float 涨停价（元） 
        dtj:float 跌停价（元） 
        jjia:float 均价（元） 
        lbi:float 量比 
        zgj:float 最高价（元） 
        zdj:float 最低价（元） 
        jrkpj:float 今日开盘价（元） 
        zrspj:float 昨日收盘价（元） 
        ssdate:String 上市日期 
        weibi:float 委比（%） 
        wpan:float 外盘（手） 
        npan:float 内盘（手） 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        mgsy:float 每股收益（元） 
        zf05:float 5日涨幅（%） 
        zf10:float 10日涨幅（%） 
        zf20:float 20日涨幅（%） 
        zf60:float 60日涨幅（%） 
        zfy:float 今年以来涨幅（%） 
        zys:float 总营收（元） 
        zystb:float 总营收同比（%） 
        jzc:float 净资产 
        jlr:float 净利润 
        mlil:float 毛利率 
        jlil:float 净利率 
        fzl:float 负债率 
        mgwfplr:float 每股未分配利润 
        mgjzc:float 每股净资产 
        mggjj:float 每股公积金（元） 
        zljlr:float 今日主力净流入（元） 
        cddlr:float 今日超大单流入（元） 
        cddlc:float 今日超大单流出（元） 
        cddjlr:float 今日超大单净流入（元） 
        ddlr:float 今日大单流入（元） 
        ddlc:float 今日大单流出（元） 
        ddjlr:float 今日大单净流入（元） 
        zdlr:float 今日中单流入（元） 
        zdlc:float 今日中单流出（元） 
        zdjlr:float 今日中单净流入（元） 
        xdlr:float 今日小单流入（元） 
        xdlc:float 今日小单流出（元） 
        xdjlr:float 今日小单净流入（元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'price' : 'float', 'zdfd' : 'float', 'zded' : 'float', 'cjl' : 'float', 'cje' : 'float', 'zhfu' : 'float', 'hslv' : 'float', 'sjlv' : 'float', 'dsyl' : 'float', 'jsyl' : 'float', 'ttmsyl' : 'float', 'ztj' : 'float', 'dtj' : 'float', 'jjia' : 'float', 'lbi' : 'float', 'zgj' : 'float', 'zdj' : 'float', 'jrkpj' : 'float', 'zrspj' : 'float', 'ssdate' : 'str', 'weibi' : 'float', 'wpan' : 'float', 'npan' : 'float', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'mgsy' : 'float', 'zf05' : 'float', 'zf10' : 'float', 'zf20' : 'float', 'zf60' : 'float', 'zfy' : 'float', 'zys' : 'float', 'zystb' : 'float', 'jzc' : 'float', 'jlr' : 'float', 'mlil' : 'float', 'jlil' : 'float', 'fzl' : 'float', 'mgwfplr' : 'float', 'mgjzc' : 'float', 'mggjj' : 'float', 'zljlr' : 'float', 'cddlr' : 'float', 'cddlc' : 'float', 'cddjlr' : 'float', 'ddlr' : 'float', 'ddlc' : 'float', 'ddjlr' : 'float', 'zdlr' : 'float', 'zdlc' : 'float', 'zdjlr' : 'float', 'xdlr' : 'float', 'xdlc' : 'float', 'xdjlr' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '股票名称', 'price': '最新价（元）', 'zdfd': '涨跌幅度（%）', 'zded': '涨跌额度（元）', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'zhfu': '振幅（%）', 'hslv': '换手率（%）', 'sjlv': '市净率（%）', 'dsyl': '市盈率（动态）', 'jsyl': '市盈率(静)', 'ttmsyl': '市盈率(TTM)', 'ztj': '涨停价（元）', 'dtj': '跌停价（元）', 'jjia': '均价（元）', 'lbi': '量比', 'zgj': '最高价（元）', 'zdj': '最低价（元）', 'jrkpj': '今日开盘价（元）', 'zrspj': '昨日收盘价（元）', 'ssdate': '上市日期', 'weibi': '委比（%）', 'wpan': '外盘（手）', 'npan': '内盘（手）', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'mgsy': '每股收益（元）', 'zf05': '5日涨幅（%）', 'zf10': '10日涨幅（%）', 'zf20': '20日涨幅（%）', 'zf60': '60日涨幅（%）', 'zfy': '今年以来涨幅（%）', 'zys': '总营收（元）', 'zystb': '总营收同比（%）', 'jzc': '净资产', 'jlr': '净利润', 'mlil': '毛利率', 'jlil': '净利率', 'fzl': '负债率', 'mgwfplr': '每股未分配利润', 'mgjzc': '每股净资产', 'mggjj': '每股公积金（元）', 'zljlr': '今日主力净流入（元）', 'cddlr': '今日超大单流入（元）', 'cddlc': '今日超大单流出（元）', 'cddjlr': '今日超大单净流入（元）', 'ddlr': '今日大单流入（元）', 'ddlc': '今日大单流出（元）', 'ddjlr': '今日大单净流入（元）', 'zdlr': '今日中单流入（元）', 'zdlc': '今日中单流出（元）', 'zdjlr': '今日中单净流入（元）', 'xdlr': '今日小单流入（元）', 'xdlc': '今日小单流出（元）', 'xdjlr': '今日小单净流入（元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHKDailyMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHKMinuteKLine(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        港股数据/分线数据
        http://www.mushuju.com/b_hkbranchdata.html
        <参数>
        code:String 股票代码，code参数可以从【港股->港股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 分时时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        cjjj:float 成交均价 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'cjjj' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '分时时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'cjjj': '成交均价'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHKMinuteKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHKHourKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        港股数据/时线数据
        http://www.mushuju.com/b_hktimelinedata.html
        <参数>
        code:String 股票代码，code参数可以从【港股->港股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        ktype:int K线类别，取值范围：5|5分钟；15|15分钟；30|30分钟；60|60分钟 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHKHourKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockHKDayKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        港股数据/日线数据
        http://www.mushuju.com/b_hkdailydata.html
        <参数>
        code:String 股票代码，code参数可以从【港股->港股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockHKDayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockUSABaseInfo(self,code: str, fields:str='all')->Query:
        """
        美股数据/美股列表
        http://www.mushuju.com/b_usstocklist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockUSABaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockUSADailyMarket(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        美股数据/每日行情
        http://www.mushuju.com/b_usdailymarket.html
        <参数>
        code:String 股票代码，code参数可以从【美股->美股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 股票名称 
        price:float 最新价（元） 
        zdfd:float 涨跌幅度（%） 
        zded:float 涨跌额度（元） 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        zhfu:float 振幅（%） 
        hslv:float 换手率（%） 
        sjlv:float 市净率（%） 
        dsyl:float 市盈率（动态） 
        jsyl:float 市盈率(静) 
        ttmsyl:float 市盈率(TTM) 
        ztj:float 涨停价（元） 
        dtj:float 跌停价（元） 
        jjia:float 均价（元） 
        lbi:float 量比 
        zgj:float 最高价（元） 
        zdj:float 最低价（元） 
        jrkpj:float 今日开盘价（元） 
        zrspj:float 昨日收盘价（元） 
        ssdate:String 上市日期 
        weibi:float 委比（%） 
        wpan:float 外盘（手） 
        npan:float 内盘（手） 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        mgsy:float 每股收益（元） 
        zf05:float 5日涨幅（%） 
        zf10:float 10日涨幅（%） 
        zf20:float 20日涨幅（%） 
        zf60:float 60日涨幅（%） 
        zfy:float 今年以来涨幅（%） 
        zys:float 总营收（元） 
        zystb:float 总营收同比（%） 
        jzc:float 净资产 
        jlr:float 净利润 
        mlil:float 毛利率 
        jlil:float 净利率 
        fzl:float 负债率 
        mgwfplr:float 每股未分配利润 
        mgjzc:float 每股净资产 
        mggjj:float 每股公积金（元） 
        zljlr:float 今日主力净流入（元） 
        cddlr:float 今日超大单流入（元） 
        cddlc:float 今日超大单流出（元） 
        cddjlr:float 今日超大单净流入（元） 
        ddlr:float 今日大单流入（元） 
        ddlc:float 今日大单流出（元） 
        ddjlr:float 今日大单净流入（元） 
        zdlr:float 今日中单流入（元） 
        zdlc:float 今日中单流出（元） 
        zdjlr:float 今日中单净流入（元） 
        xdlr:float 今日小单流入（元） 
        xdlc:float 今日小单流出（元） 
        xdjlr:float 今日小单净流入（元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'price' : 'float', 'zdfd' : 'float', 'zded' : 'float', 'cjl' : 'float', 'cje' : 'float', 'zhfu' : 'float', 'hslv' : 'float', 'sjlv' : 'float', 'dsyl' : 'float', 'jsyl' : 'float', 'ttmsyl' : 'float', 'ztj' : 'float', 'dtj' : 'float', 'jjia' : 'float', 'lbi' : 'float', 'zgj' : 'float', 'zdj' : 'float', 'jrkpj' : 'float', 'zrspj' : 'float', 'ssdate' : 'str', 'weibi' : 'float', 'wpan' : 'float', 'npan' : 'float', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'mgsy' : 'float', 'zf05' : 'float', 'zf10' : 'float', 'zf20' : 'float', 'zf60' : 'float', 'zfy' : 'float', 'zys' : 'float', 'zystb' : 'float', 'jzc' : 'float', 'jlr' : 'float', 'mlil' : 'float', 'jlil' : 'float', 'fzl' : 'float', 'mgwfplr' : 'float', 'mgjzc' : 'float', 'mggjj' : 'float', 'zljlr' : 'float', 'cddlr' : 'float', 'cddlc' : 'float', 'cddjlr' : 'float', 'ddlr' : 'float', 'ddlc' : 'float', 'ddjlr' : 'float', 'zdlr' : 'float', 'zdlc' : 'float', 'zdjlr' : 'float', 'xdlr' : 'float', 'xdlc' : 'float', 'xdjlr' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '股票名称', 'price': '最新价（元）', 'zdfd': '涨跌幅度（%）', 'zded': '涨跌额度（元）', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'zhfu': '振幅（%）', 'hslv': '换手率（%）', 'sjlv': '市净率（%）', 'dsyl': '市盈率（动态）', 'jsyl': '市盈率(静)', 'ttmsyl': '市盈率(TTM)', 'ztj': '涨停价（元）', 'dtj': '跌停价（元）', 'jjia': '均价（元）', 'lbi': '量比', 'zgj': '最高价（元）', 'zdj': '最低价（元）', 'jrkpj': '今日开盘价（元）', 'zrspj': '昨日收盘价（元）', 'ssdate': '上市日期', 'weibi': '委比（%）', 'wpan': '外盘（手）', 'npan': '内盘（手）', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'mgsy': '每股收益（元）', 'zf05': '5日涨幅（%）', 'zf10': '10日涨幅（%）', 'zf20': '20日涨幅（%）', 'zf60': '60日涨幅（%）', 'zfy': '今年以来涨幅（%）', 'zys': '总营收（元）', 'zystb': '总营收同比（%）', 'jzc': '净资产', 'jlr': '净利润', 'mlil': '毛利率', 'jlil': '净利率', 'fzl': '负债率', 'mgwfplr': '每股未分配利润', 'mgjzc': '每股净资产', 'mggjj': '每股公积金（元）', 'zljlr': '今日主力净流入（元）', 'cddlr': '今日超大单流入（元）', 'cddlc': '今日超大单流出（元）', 'cddjlr': '今日超大单净流入（元）', 'ddlr': '今日大单流入（元）', 'ddlc': '今日大单流出（元）', 'ddjlr': '今日大单净流入（元）', 'zdlr': '今日中单流入（元）', 'zdlc': '今日中单流出（元）', 'zdjlr': '今日中单净流入（元）', 'xdlr': '今日小单流入（元）', 'xdlc': '今日小单流出（元）', 'xdjlr': '今日小单净流入（元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockUSADailyMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockUSAMinuteKLine(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        美股数据/分线数据
        http://www.mushuju.com/b_usbranchdata.html
        <参数>
        code:String 股票代码，code参数可以从【美股->美股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 分时时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        cjjj:float 成交均价 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'cjjj' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '分时时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'cjjj': '成交均价'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockUSAMinuteKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockUSAHourKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        美股数据/时线数据
        http://www.mushuju.com/b_ustimelinedata.html
        <参数>
        code:String 股票代码，code参数可以从【美股->美股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        ktype:int K线类别，取值范围：5|5分钟；15|15分钟；30|30分钟；60|60分钟 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockUSAHourKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getStockUSADayKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        美股数据/日线数据
        http://www.mushuju.com/b_usdailydata.html
        <参数>
        code:String 股票代码，code参数可以从【美股->美股列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getStockUSADayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getIndexHSBaseInfo(self,code: str, fields:str='all')->Query:
        """
        指数数据/沪深指数列表
        http://www.mushuju.com/b_hsindexlist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getIndexHSBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getIndexHKBaseInfo(self,code: str, fields:str='all')->Query:
        """
        指数数据/香港指数列表
        http://www.mushuju.com/b_hkindexlist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getIndexHKBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getIndexQQBaseInfo(self,code: str, fields:str='all')->Query:
        """
        指数数据/全球指数列表
        http://www.mushuju.com/b_globalindexlist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getIndexQQBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getIndexBondBaseInfo(self,code: str, fields:str='all')->Query:
        """
        指数数据/债券指数列表
        http://www.mushuju.com/b_bondindexlist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getIndexBondBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getIndexDailyMarket(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        指数数据/每日行情
        http://www.mushuju.com/b_indexdailymarket.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 股票名称 
        price:float 最新价（元） 
        zdfd:float 涨跌幅度（%） 
        zded:float 涨跌额度（元） 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        zhfu:float 振幅（%） 
        hslv:float 换手率（%） 
        sjlv:float 市净率（%） 
        dsyl:float 市盈率（动态） 
        jsyl:float 市盈率(静) 
        ttmsyl:float 市盈率(TTM) 
        ztj:float 涨停价（元） 
        dtj:float 跌停价（元） 
        jjia:float 均价（元） 
        lbi:float 量比 
        zgj:float 最高价（元） 
        zdj:float 最低价（元） 
        jrkpj:float 今日开盘价（元） 
        zrspj:float 昨日收盘价（元） 
        ssdate:String 上市日期 
        weibi:float 委比（%） 
        wpan:float 外盘（手） 
        npan:float 内盘（手） 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        mgsy:float 每股收益（元） 
        zf05:float 5日涨幅（%） 
        zf10:float 10日涨幅（%） 
        zf20:float 20日涨幅（%） 
        zf60:float 60日涨幅（%） 
        zfy:float 今年以来涨幅（%） 
        zys:float 总营收（元） 
        zystb:float 总营收同比（%） 
        jzc:float 净资产 
        jlr:float 净利润 
        mlil:float 毛利率 
        jlil:float 净利率 
        fzl:float 负债率 
        mgwfplr:float 每股未分配利润 
        mgjzc:float 每股净资产 
        mggjj:float 每股公积金（元） 
        zljlr:float 今日主力净流入（元） 
        cddlr:float 今日超大单流入（元） 
        cddlc:float 今日超大单流出（元） 
        cddjlr:float 今日超大单净流入（元） 
        ddlr:float 今日大单流入（元） 
        ddlc:float 今日大单流出（元） 
        ddjlr:float 今日大单净流入（元） 
        zdlr:float 今日中单流入（元） 
        zdlc:float 今日中单流出（元） 
        zdjlr:float 今日中单净流入（元） 
        xdlr:float 今日小单流入（元） 
        xdlc:float 今日小单流出（元） 
        xdjlr:float 今日小单净流入（元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'price' : 'float', 'zdfd' : 'float', 'zded' : 'float', 'cjl' : 'float', 'cje' : 'float', 'zhfu' : 'float', 'hslv' : 'float', 'sjlv' : 'float', 'dsyl' : 'float', 'jsyl' : 'float', 'ttmsyl' : 'float', 'ztj' : 'float', 'dtj' : 'float', 'jjia' : 'float', 'lbi' : 'float', 'zgj' : 'float', 'zdj' : 'float', 'jrkpj' : 'float', 'zrspj' : 'float', 'ssdate' : 'str', 'weibi' : 'float', 'wpan' : 'float', 'npan' : 'float', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'mgsy' : 'float', 'zf05' : 'float', 'zf10' : 'float', 'zf20' : 'float', 'zf60' : 'float', 'zfy' : 'float', 'zys' : 'float', 'zystb' : 'float', 'jzc' : 'float', 'jlr' : 'float', 'mlil' : 'float', 'jlil' : 'float', 'fzl' : 'float', 'mgwfplr' : 'float', 'mgjzc' : 'float', 'mggjj' : 'float', 'zljlr' : 'float', 'cddlr' : 'float', 'cddlc' : 'float', 'cddjlr' : 'float', 'ddlr' : 'float', 'ddlc' : 'float', 'ddjlr' : 'float', 'zdlr' : 'float', 'zdlc' : 'float', 'zdjlr' : 'float', 'xdlr' : 'float', 'xdlc' : 'float', 'xdjlr' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '股票名称', 'price': '最新价（元）', 'zdfd': '涨跌幅度（%）', 'zded': '涨跌额度（元）', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'zhfu': '振幅（%）', 'hslv': '换手率（%）', 'sjlv': '市净率（%）', 'dsyl': '市盈率（动态）', 'jsyl': '市盈率(静)', 'ttmsyl': '市盈率(TTM)', 'ztj': '涨停价（元）', 'dtj': '跌停价（元）', 'jjia': '均价（元）', 'lbi': '量比', 'zgj': '最高价（元）', 'zdj': '最低价（元）', 'jrkpj': '今日开盘价（元）', 'zrspj': '昨日收盘价（元）', 'ssdate': '上市日期', 'weibi': '委比（%）', 'wpan': '外盘（手）', 'npan': '内盘（手）', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'mgsy': '每股收益（元）', 'zf05': '5日涨幅（%）', 'zf10': '10日涨幅（%）', 'zf20': '20日涨幅（%）', 'zf60': '60日涨幅（%）', 'zfy': '今年以来涨幅（%）', 'zys': '总营收（元）', 'zystb': '总营收同比（%）', 'jzc': '净资产', 'jlr': '净利润', 'mlil': '毛利率', 'jlil': '净利率', 'fzl': '负债率', 'mgwfplr': '每股未分配利润', 'mgjzc': '每股净资产', 'mggjj': '每股公积金（元）', 'zljlr': '今日主力净流入（元）', 'cddlr': '今日超大单流入（元）', 'cddlc': '今日超大单流出（元）', 'cddjlr': '今日超大单净流入（元）', 'ddlr': '今日大单流入（元）', 'ddlc': '今日大单流出（元）', 'ddjlr': '今日大单净流入（元）', 'zdlr': '今日中单流入（元）', 'zdlc': '今日中单流出（元）', 'zdjlr': '今日中单净流入（元）', 'xdlr': '今日小单流入（元）', 'xdlc': '今日小单流出（元）', 'xdjlr': '今日小单净流入（元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getIndexDailyMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getIndexHourKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        指数数据/时线数据
        http://www.mushuju.com/b_indextimelinedata.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        ktype:int K线类别，取值范围：5|5分钟；15|15分钟；30|30分钟；60|60分钟 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getIndexHourKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getIndexDayKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        指数数据/日线数据
        http://www.mushuju.com/b_indexdailydata.html
        <参数>
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getIndexDayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getBondHSBaseInfo(self,code: str, fields:str='all')->Query:
        """
        债券数据/可转债列表
        http://www.mushuju.com/b_convertiblebonds.html
        <参数>
        code:String 债券代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getBondHSBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getBondHSDailyMarket(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        债券数据/每日行情
        http://www.mushuju.com/b_bondsdailymarket.html
        <参数>
        code:String 股票代码，code参数可以从【债券->可转债列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 股票名称 
        price:float 最新价（元） 
        zdfd:float 涨跌幅度（%） 
        zded:float 涨跌额度（元） 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        zhfu:float 振幅（%） 
        hslv:float 换手率（%） 
        sjlv:float 市净率（%） 
        dsyl:float 市盈率（动态） 
        jsyl:float 市盈率(静) 
        ttmsyl:float 市盈率(TTM) 
        ztj:float 涨停价（元） 
        dtj:float 跌停价（元） 
        jjia:float 均价（元） 
        lbi:float 量比 
        zgj:float 最高价（元） 
        zdj:float 最低价（元） 
        jrkpj:float 今日开盘价（元） 
        zrspj:float 昨日收盘价（元） 
        ssdate:String 上市日期 
        weibi:float 委比（%） 
        wpan:float 外盘（手） 
        npan:float 内盘（手） 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        mgsy:float 每股收益（元） 
        zf05:float 5日涨幅（%） 
        zf10:float 10日涨幅（%） 
        zf20:float 20日涨幅（%） 
        zf60:float 60日涨幅（%） 
        zfy:float 今年以来涨幅（%） 
        zys:float 总营收（元） 
        zystb:float 总营收同比（%） 
        jzc:float 净资产 
        jlr:float 净利润 
        mlil:float 毛利率 
        jlil:float 净利率 
        fzl:float 负债率 
        mgwfplr:float 每股未分配利润 
        mgjzc:float 每股净资产 
        mggjj:float 每股公积金（元） 
        zljlr:float 今日主力净流入（元） 
        cddlr:float 今日超大单流入（元） 
        cddlc:float 今日超大单流出（元） 
        cddjlr:float 今日超大单净流入（元） 
        ddlr:float 今日大单流入（元） 
        ddlc:float 今日大单流出（元） 
        ddjlr:float 今日大单净流入（元） 
        zdlr:float 今日中单流入（元） 
        zdlc:float 今日中单流出（元） 
        zdjlr:float 今日中单净流入（元） 
        xdlr:float 今日小单流入（元） 
        xdlc:float 今日小单流出（元） 
        xdjlr:float 今日小单净流入（元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'price' : 'float', 'zdfd' : 'float', 'zded' : 'float', 'cjl' : 'float', 'cje' : 'float', 'zhfu' : 'float', 'hslv' : 'float', 'sjlv' : 'float', 'dsyl' : 'float', 'jsyl' : 'float', 'ttmsyl' : 'float', 'ztj' : 'float', 'dtj' : 'float', 'jjia' : 'float', 'lbi' : 'float', 'zgj' : 'float', 'zdj' : 'float', 'jrkpj' : 'float', 'zrspj' : 'float', 'ssdate' : 'str', 'weibi' : 'float', 'wpan' : 'float', 'npan' : 'float', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'mgsy' : 'float', 'zf05' : 'float', 'zf10' : 'float', 'zf20' : 'float', 'zf60' : 'float', 'zfy' : 'float', 'zys' : 'float', 'zystb' : 'float', 'jzc' : 'float', 'jlr' : 'float', 'mlil' : 'float', 'jlil' : 'float', 'fzl' : 'float', 'mgwfplr' : 'float', 'mgjzc' : 'float', 'mggjj' : 'float', 'zljlr' : 'float', 'cddlr' : 'float', 'cddlc' : 'float', 'cddjlr' : 'float', 'ddlr' : 'float', 'ddlc' : 'float', 'ddjlr' : 'float', 'zdlr' : 'float', 'zdlc' : 'float', 'zdjlr' : 'float', 'xdlr' : 'float', 'xdlc' : 'float', 'xdjlr' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '股票名称', 'price': '最新价（元）', 'zdfd': '涨跌幅度（%）', 'zded': '涨跌额度（元）', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'zhfu': '振幅（%）', 'hslv': '换手率（%）', 'sjlv': '市净率（%）', 'dsyl': '市盈率（动态）', 'jsyl': '市盈率(静)', 'ttmsyl': '市盈率(TTM)', 'ztj': '涨停价（元）', 'dtj': '跌停价（元）', 'jjia': '均价（元）', 'lbi': '量比', 'zgj': '最高价（元）', 'zdj': '最低价（元）', 'jrkpj': '今日开盘价（元）', 'zrspj': '昨日收盘价（元）', 'ssdate': '上市日期', 'weibi': '委比（%）', 'wpan': '外盘（手）', 'npan': '内盘（手）', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'mgsy': '每股收益（元）', 'zf05': '5日涨幅（%）', 'zf10': '10日涨幅（%）', 'zf20': '20日涨幅（%）', 'zf60': '60日涨幅（%）', 'zfy': '今年以来涨幅（%）', 'zys': '总营收（元）', 'zystb': '总营收同比（%）', 'jzc': '净资产', 'jlr': '净利润', 'mlil': '毛利率', 'jlil': '净利率', 'fzl': '负债率', 'mgwfplr': '每股未分配利润', 'mgjzc': '每股净资产', 'mggjj': '每股公积金（元）', 'zljlr': '今日主力净流入（元）', 'cddlr': '今日超大单流入（元）', 'cddlc': '今日超大单流出（元）', 'cddjlr': '今日超大单净流入（元）', 'ddlr': '今日大单流入（元）', 'ddlc': '今日大单流出（元）', 'ddjlr': '今日大单净流入（元）', 'zdlr': '今日中单流入（元）', 'zdlc': '今日中单流出（元）', 'zdjlr': '今日中单净流入（元）', 'xdlr': '今日小单流入（元）', 'xdlc': '今日小单流出（元）', 'xdjlr': '今日小单净流入（元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getBondHSDailyMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getBondHSHourKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        债券数据/时线数据
        http://www.mushuju.com/b_bondstimelinedata.html
        <参数>
        code:String 股票代码，code参数可以从【债券->可转债列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；不支持all参数查询。 Yes
        ktype:int K线类别，取值范围：5|5分钟；15|15分钟；30|30分钟；60|60分钟 Yes
        startDate:String 开始日期，yyyy-MM-dd HH:mm:ss格式，例如：2020-01-01 01:00:00 Yes
        endDate:String 结束日期，yyyy-MM-dd HH:mm:ss格式，例如：2050-01-01 01:00:00 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getBondHSHourKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getBondHSDayKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        债券数据/日线数据
        http://www.mushuju.com/b_bondsdailydata.html
        <参数>
        code:String 股票代码，code参数可以从【债券->可转债列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getBondHSDayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getWatchStockTimeKLine(self,type: int, code: str, fields:str='all')->Query:
        """
        实时数据/分线实时数据
        http://www.mushuju.com/b_realtimedata.html
        <参数>
        type:int 资产类型，取值范围：1|沪深京A股；2|沪深京B股；3|港股；4|美股；5|黄金；6|汇率；7|Reits；10|沪深指数；11|香港指数；12|全球指数；13|债券指数；20|场内基金；30|沪深债券；40|行业板块；41|概念板块；42|地域板块 Yes
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        tdate:String 交易时间 
        name:String 股票名称 
        price:float 最新价（元） 
        high:float 最高价（元） 
        low:float 最低价（元） 
        open:float 今日开盘价（元） 
        zrspj:float 昨日收盘价（元） 
        zdfd:float 涨跌幅度（%） 
        zded:float 涨跌额度（元） 
        cjl:float 成交量（手） 
        cje:float 成交额（元） 
        zhfu:float 振幅（%） 
        hslv:float 换手率（%） 
        sjlv:float 市净率（%） 
        jjia:float 均价（元） 
        lbi:float 量比 
        weibi:float 委比（%） 
        wpan:float 外盘（手） 
        npan:float 内盘（手） 
        dsyl:float 市盈率（动态） 
        jsyl:float 市盈率(静) 
        ttmsyl:float 市盈率(TTM) 
        ztj:float 涨停价（元） 
        dtj:float 跌停价（元） 
        ssdate:String 上市日期 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        mgsy:float 每股收益（元） 
        zf05:float 5日涨幅（%） 
        zf10:float 10日涨幅（%） 
        zf20:float 20日涨幅（%） 
        zf60:float 60日涨幅（%） 
        zfy:float 今年以来涨幅（%） 
        zys:float 总营收（元） 
        zystb:float 总营收同比（%） 
        jzc:float 净资产 
        jlr:float 净利润 
        mlil:float 毛利率 
        jlil:float 净利率 
        fzl:float 负债率 
        mgwfplr:float 每股未分配利润 
        mgjzc:float 每股净资产 
        mggjj:float 每股公积金（元） 
        zljlr:float 今日主力净流入（元） 
        cddlr:float 今日超大单流入（元） 
        cddlc:float 今日超大单流出（元） 
        cddjlr:float 今日超大单净流入（元） 
        ddlr:float 今日大单流入（元） 
        ddlc:float 今日大单流出（元） 
        ddjlr:float 今日大单净流入（元） 
        zdlr:float 今日中单流入（元） 
        zdlc:float 今日中单流出（元） 
        zdjlr:float 今日中单净流入（元） 
        xdlr:float 今日小单流入（元） 
        xdlc:float 今日小单流出（元） 
        xdjlr:float 今日小单净流入（元） 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'tdate' : 'str', 'name' : 'str', 'price' : 'float', 'high' : 'float', 'low' : 'float', 'open' : 'float', 'zrspj' : 'float', 'zdfd' : 'float', 'zded' : 'float', 'cjl' : 'float', 'cje' : 'float', 'zhfu' : 'float', 'hslv' : 'float', 'sjlv' : 'float', 'jjia' : 'float', 'lbi' : 'float', 'weibi' : 'float', 'wpan' : 'float', 'npan' : 'float', 'dsyl' : 'float', 'jsyl' : 'float', 'ttmsyl' : 'float', 'ztj' : 'float', 'dtj' : 'float', 'ssdate' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'mgsy' : 'float', 'zf05' : 'float', 'zf10' : 'float', 'zf20' : 'float', 'zf60' : 'float', 'zfy' : 'float', 'zys' : 'float', 'zystb' : 'float', 'jzc' : 'float', 'jlr' : 'float', 'mlil' : 'float', 'jlil' : 'float', 'fzl' : 'float', 'mgwfplr' : 'float', 'mgjzc' : 'float', 'mggjj' : 'float', 'zljlr' : 'float', 'cddlr' : 'float', 'cddlc' : 'float', 'cddjlr' : 'float', 'ddlr' : 'float', 'ddlc' : 'float', 'ddjlr' : 'float', 'zdlr' : 'float', 'zdlc' : 'float', 'zdjlr' : 'float', 'xdlr' : 'float', 'xdlc' : 'float', 'xdjlr' : 'float'}
        column_name_dict = {'code': '股票代码', 'tdate': '交易时间', 'name': '股票名称', 'price': '最新价（元）', 'high': '最高价（元）', 'low': '最低价（元）', 'open': '今日开盘价（元）', 'zrspj': '昨日收盘价（元）', 'zdfd': '涨跌幅度（%）', 'zded': '涨跌额度（元）', 'cjl': '成交量（手）', 'cje': '成交额（元）', 'zhfu': '振幅（%）', 'hslv': '换手率（%）', 'sjlv': '市净率（%）', 'jjia': '均价（元）', 'lbi': '量比', 'weibi': '委比（%）', 'wpan': '外盘（手）', 'npan': '内盘（手）', 'dsyl': '市盈率（动态）', 'jsyl': '市盈率(静)', 'ttmsyl': '市盈率(TTM)', 'ztj': '涨停价（元）', 'dtj': '跌停价（元）', 'ssdate': '上市日期', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'mgsy': '每股收益（元）', 'zf05': '5日涨幅（%）', 'zf10': '10日涨幅（%）', 'zf20': '20日涨幅（%）', 'zf60': '60日涨幅（%）', 'zfy': '今年以来涨幅（%）', 'zys': '总营收（元）', 'zystb': '总营收同比（%）', 'jzc': '净资产', 'jlr': '净利润', 'mlil': '毛利率', 'jlil': '净利率', 'fzl': '负债率', 'mgwfplr': '每股未分配利润', 'mgjzc': '每股净资产', 'mggjj': '每股公积金（元）', 'zljlr': '今日主力净流入（元）', 'cddlr': '今日超大单流入（元）', 'cddlc': '今日超大单流出（元）', 'cddjlr': '今日超大单净流入（元）', 'ddlr': '今日大单流入（元）', 'ddlc': '今日大单流出（元）', 'ddjlr': '今日大单净流入（元）', 'zdlr': '今日中单流入（元）', 'zdlc': '今日中单流出（元）', 'zdjlr': '今日中单净流入（元）', 'xdlr': '今日小单流入（元）', 'xdlc': '今日小单流出（元）', 'xdjlr': '今日小单净流入（元）'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getWatchStockTimeKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getTSStockPosition(self,type: int, code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        其他数据/反向持仓
        http://www.mushuju.com/b_reverseposition.html
        <参数>
        type:int 资产类型，取值范围：1|沪深京A股；2|沪深京B股；3|港股 Yes
        code:String 股票代码，code参数可以从【通用接口->股票列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        tdate:String 基金公布持仓季度 
        fundnum:int 持仓该股票的基金个数 
        fundsz:float 基金持该股票总市值（万元） 
        fundcgs:float 基金持有该股票总股数（万股） 
        fundbl:float 基金持有该股票比例(持有股数/流通股数) 
        ltgb:float 流通股本（万股） 
        zgb:float 总股本（万股） 
        ltsz:float 流通市值（万元） 
        zsz:float 总市值（万元） 
        ssdate:String 上市日期 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'tdate' : 'str', 'fundnum' : 'int', 'fundsz' : 'float', 'fundcgs' : 'float', 'fundbl' : 'float', 'ltgb' : 'float', 'zgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'tdate': '基金公布持仓季度', 'fundnum': '持仓该股票的基金个数', 'fundsz': '基金持该股票总市值（万元）', 'fundcgs': '基金持有该股票总股数（万股）', 'fundbl': '基金持有该股票比例(持有股数/流通股数)', 'ltgb': '流通股本（万股）', 'zgb': '总股本（万股）', 'ltsz': '流通市值（万元）', 'zsz': '总市值（万元）', 'ssdate': '上市日期'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getTSStockPosition",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getGoldBaseInfo(self,code: str, fields:str='all')->Query:
        """
        黄金数据/黄金列表
        http://www.mushuju.com/b_goldlist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getGoldBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getGoldDayKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        黄金数据/日线数据
        http://www.mushuju.com/b_golddailydata.html
        <参数>
        code:String 股票代码，code参数可以从【其它->黄金->黄金列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getGoldDayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getWaihuiBaseInfo(self,code: str, fields:str='all')->Query:
        """
        外汇数据/外汇列表
        http://www.mushuju.com/b_foreignexchangelist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getWaihuiBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getWaihuiDayKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        外汇数据/日线数据
        http://www.mushuju.com/b_foreigndailydata.html
        <参数>
        code:String 股票代码，code参数可以从【其它->外汇->外汇列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getWaihuiDayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getReitsBaseInfo(self,code: str, fields:str='all')->Query:
        """
        REITS数据/REITS列表
        http://www.mushuju.com/b_reitslist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        stype:int 股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股 
        hsgt:int 沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港) 
        bk:String 所属板块，个股包括主板、创业板、科创板 
        roe:float ROE 
        zgb:float 总股本（股） 
        ltgb:float 流通股本（股） 
        ltsz:float 流通市值（元） 
        zsz:float 总市值（元） 
        ssdate:String 上市日期 
        z50:String 归属行业板块名称 
        z52:String 归属地域板块名称 
        z53:String 归属概念板块名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'stype' : 'int', 'hsgt' : 'int', 'bk' : 'str', 'roe' : 'float', 'zgb' : 'float', 'ltgb' : 'float', 'ltsz' : 'float', 'zsz' : 'float', 'ssdate' : 'str', 'z50' : 'str', 'z52' : 'str', 'z53' : 'str'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'stype': '股票类型，1：深证股票，2：上证股票，3：北证股票，4：港股', 'hsgt': '沪深港通，1：沪股通(港>沪)、2：深股通(港>深)、3：港股通(沪>港)、4：港股通(深>港)、5：港股通(深>港或沪>港)', 'bk': '所属板块，个股包括主板、创业板、科创板', 'roe': 'ROE', 'zgb': '总股本（股）', 'ltgb': '流通股本（股）', 'ltsz': '流通市值（元）', 'zsz': '总市值（元）', 'ssdate': '上市日期', 'z50': '归属行业板块名称', 'z52': '归属地域板块名称', 'z53': '归属概念板块名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getReitsBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getReitsDayKLine(self,code: str, ktype: int, startDate: str, endDate: str, fields:str='all')->Query:
        """
        REITS数据/日线数据
        http://www.mushuju.com/b_reitsdailydata.html
        <参数>
        code:String 股票代码，code参数可以从【其它->REITS->REITS列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        ktype:int K线类别，取值范围：101|日线；102|周线；103|月线 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        code:String 股票代码 
        name:String 股票名称 
        ktype:int K线类别 
        fq:int 复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权 
        tdate:String 交易时间 
        open:float 开盘价 
        close:float 收盘价 
        high:float 最高价 
        low:float 最低价 
        cjl:float 成交量 
        cje:float 成交额 
        hsl:float 换手率 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'code' : 'str', 'name' : 'str', 'ktype' : 'int', 'fq' : 'int', 'tdate' : 'str', 'open' : 'float', 'close' : 'float', 'high' : 'float', 'low' : 'float', 'cjl' : 'float', 'cje' : 'float', 'hsl' : 'float'}
        column_name_dict = {'code': '股票代码', 'name': '股票名称', 'ktype': 'K线类别', 'fq': '复权信息，除沪深京A股、B股、港股外，其余复权值默认为前复权', 'tdate': '交易时间', 'open': '开盘价', 'close': '收盘价', 'high': '最高价', 'low': '最低价', 'cjl': '成交量', 'cje': '成交额', 'hsl': '换手率'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getReitsDayKLine",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getQihuoBaseInfo(self,code: str, fields:str='all')->Query:
        """
        期货数据/期货列表
        http://www.mushuju.com/b_futureslist.html
        <参数>
        code:String 股票代码，支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        jyscode:int 交易所代码 
        jysname:String 交易所名称 
        pzcode:String 品种代码 
        code:String 合约代码 
        name:String 合约名称 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'jyscode' : 'int', 'jysname' : 'str', 'pzcode' : 'str', 'code' : 'str', 'name' : 'str'}
        column_name_dict = {'jyscode': '交易所代码', 'jysname': '交易所名称', 'pzcode': '品种代码', 'code': '合约代码', 'name': '合约名称'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getQihuoBaseInfo",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    


    def getQihuoDailyMarket(self,code: str, startDate: str, endDate: str, fields:str='all')->Query:
        """
        期货数据/每日行情
        http://www.mushuju.com/b_futuresdailymarket.html
        <参数>
        code:String 股票代码，code参数可以从【其它->期货->期货列表】接口中批量获取，也可以打开左上角菜单栏【数据浏览器】开关查询数据；支持批量查询，用逗号分隔，每次最多50个；若为all，则表示全部，即可获取任意一天内的所有数据。 Yes
        startDate:String 开始日期，yyyy-MM-dd格式，例如：2020-01-01 Yes
        endDate:String 结束日期，yyyy-MM-dd格式，例如：2050-01-01 Yes
        fields:String 数据字段，多个字段之间使用逗号分隔，若获取所有字段，则取值为all。 Yes
        <返回结果>
        jyscode:int 交易所代码 
        jysname:String 交易所名称 
        pzcode:String 品种代码 
        code:String 合约代码 
        name:String 合约名称 
        tdate:String 交易时间 
        zxj:float 最新价 
        zde:float 涨跌额 
        zdf:float 涨跌幅 
        jk:float 今开 
        zg:float 最高 
        zd:float 最低 
        zjsj:float 昨结 
        cjl:float 成交量 
        cje:float 成交额 
        wp:float 买盘(外盘) 
        np:float 卖盘(内盘) 
        ccl:float 持仓量 
        """
        params = locals().copy()
        real_params = {}
        column_type_dict = {'jyscode' : 'int', 'jysname' : 'str', 'pzcode' : 'str', 'code' : 'str', 'name' : 'str', 'tdate' : 'str', 'zxj' : 'float', 'zde' : 'float', 'zdf' : 'float', 'jk' : 'float', 'zg' : 'float', 'zd' : 'float', 'zjsj' : 'float', 'cjl' : 'float', 'cje' : 'float', 'wp' : 'float', 'np' : 'float', 'ccl' : 'float'}
        column_name_dict = {'jyscode': '交易所代码', 'jysname': '交易所名称', 'pzcode': '品种代码', 'code': '合约代码', 'name': '合约名称', 'tdate': '交易时间', 'zxj': '最新价', 'zde': '涨跌额', 'zdf': '涨跌幅', 'jk': '今开', 'zg': '最高', 'zd': '最低', 'zjsj': '昨结', 'cjl': '成交量', 'cje': '成交额', 'wp': '买盘(外盘)', 'np': '卖盘(内盘)', 'ccl': '持仓量'}
        for k, v in params.items():
            if v is not None and  k !='self':
                real_params[k] = v
        return Query(method="getQihuoDailyMarket",fields=fields, params=real_params,return_column_type_dict=column_type_dict,return_column_name_dict=column_name_dict,token=self.token)    
    