from diskcache import Cache
from os.path import expanduser

home = expanduser("~").replace("\\", "/")
cache = Cache(directory=home + "/.musdk", timeout=60, sqlite_synchronous=0, tag_index=True, eviction_policy='least-recently-stored',
              cull_limit=0,
              size_limit=2 ** 33)

__all__ = ['cache']
