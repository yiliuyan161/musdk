
幕数据非官方数据SDK,支持数据缓存，写入结果到数据库

## 安装

```shell
pip install git+https://gitee.com/yiliuyan161/musdk.git --upgrade
```

## 依赖

幕数据 token

## 使用

### 初始化

```python
from musdk import MuClient, Store

mu = MuClient("XXXX")  # 第一次初始化，后续从config自动加载
store = Store("postgresql+psycopg2://user:password@host:port/database")  # 第一次初始化，后续从config自动加载
mu.store_config()  # 调用后,下次调用不用再次输入token
store.store_config()  # 调用后,下次调用不用再次输入con_url
```

### 同步方式查询

```python
from musdk import MuClient

mu = MuClient()
# 查询股票基本信息
mu.getBaseInfo(type=1,code='all').get(cn_column_names=True)

```

### 异步方式查询

```python
from musdk import MuClient

mu = MuClient()
# 查询股票基本信息
await mu.getBaseInfo(type=1,code='all').async_get(cn_column_names=True)
```

### 数据保存到postgres

```python
from musdk import MuClient, Store

mu = MuClient()
store = Store()

base_info = await mu.getBaseInfo(type=1,code='all').async_get(cn_column_names=True)
# 保存
store.save_df(base_info, 'base_info', unique_cols=['code'])
```

